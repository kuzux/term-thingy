#pragma once
#include "pch.h"
#include "ScreenBuffer.h"
#include "VT100Stream.h"
#include "Utf8Stream.h"

#include <poll.h>
#include <termios.h>

class PtyDevice
{
public:
    enum class PollResult
    {
        HasData,
        NoData,
        HungUp
    };

    PtyDevice();
    ~PtyDevice();

    void attach(std::string const& shell_path);
    bool attached() const { return m_master_fd != 0; }
    PollResult poll_data();
    size_t read_into(ScreenBuffer& screen);
    void write_line(LineBuffer& line);
    void write_zero_terminated(char const* text);
    void send_byte(uint8_t byte);

    bool in_canonical_mode() const;
    bool in_echo_mode() const;
    bool has_output_processing() const;

    void notify_size(glm::ivec2 size);
    void set_working_directory(std::string const& path);
    void set_terminal_name(std::string const& name);
private:
    static void child_process_main(const char* shell_path, const char* working_directory, const char* term_env_var);
    size_t write_char_ptr(char const* text, size_t len);

    void send_ok_status();
    void send_cursor_position(glm::ivec2 pos);

    int m_master_fd { 0 }; // Not attached when == 0
    int m_child_pid { 0 };
    struct pollfd m_pty_pfd;
    VT100Stream m_escape_stream;
    Utf8Stream m_utf_stream;
    struct termios m_termios;

    std::optional<std::string> m_working_directory;
    std::string m_terminal_name { "" };
};
