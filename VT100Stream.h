#pragma once
#include "pch.h"

// TODO: Handle these "friend" cases with a badge class
class VT100Stream;
class VT100Result;

class VT100EscapeSequence
{
    friend class VT100Result;
public:
    enum class Type {
        Unknown,

        SetCursorPosition,
        MoveCursorUp,
        MoveCursorDown,
        MoveCursorLeft,
        MoveCursorRight,

        EraseInScreen,
        EraseInLine,

        EraseCharacters, // rest of the line does not move
        DeleteCharacters, // rest of the line moves

        InsertLines,
        DeleteLines,

        SetForegroundColor,
        SetBackgroundColor,
        ResetGraphicsMode,

        DeviceStatusReport,
        CursorPositionReport
    };

    Type type() const { return m_type; }
    int arg1() const { return m_arg1; }
    int arg2() const { return m_arg2; }

    ~VT100EscapeSequence() { }
private:
    VT100EscapeSequence(std::vector<uint8_t>& bytes);

    int m_arg1 { 0 };
    int m_arg2 { 0 };
    Type m_type { Type::Unknown };
};

class VT100Result
{
    friend class VT100Stream;
public:
    enum class Type {
        // you need to flush all the bytes from the stream for these cases
        EscapeSequence,
        InvalidEscapeSequence,
        OperatingSystemCommand,

        Byte,
        NoResult
    };

    Type type() const { return m_type; }
    uint8_t byte() const { assert(m_type == Type::Byte); return m_byte; }
    size_t flush_buffer(std::vector<uint8_t>& out_buffer);

    bool has_result() const { return m_type != Type::NoResult; }
    bool needs_flushing() const { return (m_type != Type::Byte && m_type != Type::NoResult) && !m_has_flushed; }
    VT100EscapeSequence escape_sequence();

    ~VT100Result();
private:
    VT100Result(VT100Stream& stream, Type type, uint8_t byte);

    VT100Stream& m_stream;
    Type m_type;
    uint8_t m_byte;
    bool m_has_flushed { false };
};

class VT100Stream
{
    friend class VT100Result;
public:
    constexpr static size_t BufferSize = 200;

    VT100Stream() { }
    ~VT100Stream() { }
    [[nodiscard]] VT100Result feed(uint8_t byte); 
private:
    enum class ParseState {
        Default,
        SawEscape,
        SawCsi,
        SawOsSeq,
        SawQuestionMark
    };

    void add_to_escape_buffer(uint8_t byte);
    VT100Result create_result(VT100Result::Type type) { return { *this, type, 0 }; }
    VT100Result create_result(uint8_t byte) { return { *this, VT100Result::Type::Byte, byte }; }
    VT100Result create_result() { return { *this, VT100Result::Type::NoResult, 0 }; }

    ParseState m_parse_state { ParseState::Default };
    std::array<uint8_t, BufferSize> m_escape_buffer;
    size_t m_buffer_position { 0 };
};
