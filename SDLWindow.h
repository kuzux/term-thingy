#pragma once
#include "pch.h"
#include <SDL.h>

class SDLEventLoop;
class SDLWindow
{
    friend class SDLEventLoop;
public:
    SDLWindow(std::string_view title, glm::ivec2 size);
    ~SDLWindow();

    void draw();
    glm::ivec2 window_size() const { return m_window_size; }
    void set_window_size(glm::ivec2 size) { m_window_size = size; }
    void set_title(std::string_view title);
    void set_icon(std::string const& filename);
    SDLEventLoop& event_loop();
private:
    SDL_Window *window = nullptr;
    SDL_GLContext ctx;

    glm::ivec2 m_window_size;
    SDL_Surface* m_window_icon { nullptr };
    SDLEventLoop* m_event_loop { nullptr };
    bool m_vsynced { false };
};

class SDLEventLoop
{
    friend class SDLWindow;
public:
    void run(std::function<void(uint64_t)> body);
    void register_event(SDL_EventType type, std::function<void(SDL_Event)> callback);
    void stop();
private:
    SDLEventLoop(SDLWindow& window);

    bool m_running { false };
    std::unordered_map<SDL_EventType, std::function<void(SDL_Event)>> m_event_cbs;
    SDLWindow& m_window;
};
