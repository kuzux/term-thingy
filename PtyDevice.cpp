#include "pch.h"
#include "PtyDevice.h"

#include <unistd.h>
#if __APPLE__
    #include <util.h>
    #include <crt_externs.h>
    #define environ (*_NSGetEnviron())
#else
    #include <pty.h>
#endif // __APPLE__
#include <sys/wait.h>
#include <sys/ioctl.h>

using namespace std;

// super-duper global set by the cli args
extern bool QUIET_MODE;

PtyDevice::PtyDevice() { }
PtyDevice::~PtyDevice()
{
    if(m_master_fd) {
        close(m_master_fd);
        int status;
        waitpid(m_child_pid, &status, WNOHANG);
        if(WIFEXITED(status))
            fmt::print(stderr, "Shell process exited normally with status {}\n", WEXITSTATUS(status));
        else if(WIFSIGNALED(status))
            fmt::print(stderr, "Shell process terminated with signal {}\n", WTERMSIG(status));
    }
}

void PtyDevice::child_process_main(const char* shell_path, const char* working_directory, const char* term_env_var)
{
    // Make sure we have no input/output buffering
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    array<const char*, 2> shell_argv = {{ shell_path, NULL }};

    vector<const char*> shell_envp;
    char** env = environ;
    while(*env) {
        if(strncmp(*env, "SHELL=", 6) == 0 || strncmp(*env, "TERM=", 5) == 0) {
            env++;
            continue;
        }
        shell_envp.push_back(*env++);
    }
    shell_envp.push_back(fmt::format("SHELL={}", shell_path).c_str());
    shell_envp.push_back(fmt::format("TERM={}", term_env_var).c_str());
    shell_envp.push_back(NULL);

    if(working_directory) chdir(working_directory);

    execve(shell_path, const_cast<char* const*>(shell_argv.data()), const_cast<char* const*>(shell_envp.data()));
}

void PtyDevice::attach(string const& shell_path)
{
    pid_t pid = forkpty(&m_master_fd, NULL, NULL, NULL);

    assert(pid >= 0);
    if(pid == 0) {
        const char* working_directory = nullptr;
        if(m_working_directory) working_directory = m_working_directory->c_str();
        child_process_main(shell_path.c_str(), working_directory, m_terminal_name.c_str());
        return;
    }
    m_child_pid = pid;
    m_pty_pfd = { m_master_fd, POLLIN, 0 };
}

void PtyDevice::set_working_directory(string const& path)
{
    m_working_directory = path;
}

void PtyDevice::set_terminal_name(string const& name)
{
    m_terminal_name = name;
}

PtyDevice::PollResult PtyDevice::poll_data()
{
    if(!attached()) return PollResult::NoData;

    int rc = tcgetattr(m_master_fd, &m_termios);
    assert(!rc);

    int poll_rc = poll(&m_pty_pfd, 1, 0);
    if(poll_rc == 0) return PollResult::NoData;
    assert(poll_rc > 0);
    
    assert(!(m_pty_pfd.revents & POLLERR));

    if(m_pty_pfd.revents & POLLHUP) {
        return PollResult::HungUp;
    } else if(m_pty_pfd.revents & POLLIN) {
        return PollResult::HasData;
    } 

    return PollResult::NoData;
}

// TODO: Somehow make sure this is only called on an fd that can be read
size_t PtyDevice::read_into(ScreenBuffer& screen_buffer)
{
    assert(attached());

    static array<uint8_t, 4096> read_buffer;

    ssize_t total_read = read(m_master_fd, read_buffer.data(), read_buffer.size());
    assert(total_read >= 0);
    if(!QUIET_MODE) fmt::print("Read {} bytes\n", total_read);

    static vector<uint8_t> tmp_escape_sequence;
    tmp_escape_sequence.reserve(VT100Stream::BufferSize+1);

    int n = 0;

    for(size_t i=0; i<(size_t)total_read; i++) {
        auto res = m_escape_stream.feed(read_buffer[i]);
        if(!res.has_result()) continue;
        if(res.type() == VT100Result::Type::EscapeSequence) {
            auto sequence = res.escape_sequence();
            switch(sequence.type()) {
            case VT100EscapeSequence::Type::Unknown:
                break;
            case VT100EscapeSequence::Type::SetCursorPosition:
                screen_buffer.set_cursor_position({ sequence.arg2()-1, sequence.arg1()-1 });
                break;
            case VT100EscapeSequence::Type::EraseInScreen:
                assert(sequence.arg1() <= 3);
                if(sequence.arg1() == 0) screen_buffer.clear_to_end();
                else if(sequence.arg1() == 1) screen_buffer.clear_to_beginning();
                else screen_buffer.clear_screen();
                break;
            case VT100EscapeSequence::Type::EraseInLine:
                assert(sequence.arg1() < 3);
                if(sequence.arg1() == 0) screen_buffer.clear_line_to_end();
                else if(sequence.arg1() == 1) screen_buffer.clear_line_to_beginning();
                else screen_buffer.clear_line();
                break;
            case VT100EscapeSequence::Type::MoveCursorUp:
                n = sequence.arg1();
                while(n != 0) {
                    screen_buffer.move_up(); n--;
                }
                break;
            case VT100EscapeSequence::Type::MoveCursorDown:
                n = sequence.arg1();
                while(n != 0) {
                    screen_buffer.move_down(); n--;
                }
                break;
            case VT100EscapeSequence::Type::MoveCursorLeft:
                n = sequence.arg1();
                while(n != 0) {
                    screen_buffer.move_left(); n--;
                }
                break;
            case VT100EscapeSequence::Type::MoveCursorRight:
                n = sequence.arg1();
                while(n != 0) {
                    screen_buffer.move_right(); n--;
                }
                break;
            case VT100EscapeSequence::Type::SetForegroundColor:
                screen_buffer.set_foreground_color(sequence.arg1());
                break;
            case VT100EscapeSequence::Type::SetBackgroundColor:
                screen_buffer.set_background_color(sequence.arg1());
                break;
            case VT100EscapeSequence::Type::ResetGraphicsMode:
                screen_buffer.reset_rendition_params();
                break;
            case VT100EscapeSequence::Type::EraseCharacters:
                screen_buffer.erase_characters(sequence.arg1());
                break;
            case VT100EscapeSequence::Type::DeleteCharacters:
                screen_buffer.delete_characters(sequence.arg1());
                break;
            case VT100EscapeSequence::Type::InsertLines:
                screen_buffer.insert_empty_lines(sequence.arg1());
                break;
            case VT100EscapeSequence::Type::DeleteLines:
                screen_buffer.delete_lines(sequence.arg1());
                break;
            case VT100EscapeSequence::Type::DeviceStatusReport:
                send_ok_status();
                break;
            case VT100EscapeSequence::Type::CursorPositionReport:
                send_cursor_position(screen_buffer.cursor_position_1_indexed());
                break;
            default:
                assert(false && "Unhandled VT100 Escape sequence");
            }
            continue;
        } else if(res.needs_flushing()) {
            auto len = res.flush_buffer(tmp_escape_sequence);
            fmt::print("Got invalid escape sequence of length {}, sequence \\x1b{}\n", len, reinterpret_cast<const char*>(tmp_escape_sequence.data()+1));
            // ignore it for now
            tmp_escape_sequence.clear();
            continue;
        }
        auto byte = res.byte();

        auto utf_res = m_utf_stream.feed(byte);
        if(!utf_res.has_result()) continue;

        if(utf_res.type() == Utf8Result::Type::InvalidSequence) {
            // Try to interpret the bytes as codepoints between 127 and 255
            for(uint8_t ch : utf_res.invalid_bytes())
                screen_buffer.insert_char((char32_t)ch);
            continue;
        }

        auto cp = utf_res.codepoint();

        if(cp == 0) break;
        else screen_buffer.insert_char(cp);
    }

    return total_read;
}

void PtyDevice::notify_size(glm::ivec2 size)
{
    if(!attached()) return;

    struct winsize ws = { (uint16_t)size.y, (uint16_t)size.x, 0, 0 };
    int rc = ioctl(m_master_fd, TIOCSWINSZ, &ws);
    assert(!rc);
}

bool PtyDevice::in_canonical_mode() const
{
    if(!attached()) return false;
    return (m_termios.c_lflag & ICANON) != 0;
}

bool PtyDevice::in_echo_mode() const
{
    if(!attached()) return true;
    return (m_termios.c_lflag & ECHO) != 0;
}

bool PtyDevice::has_output_processing() const
{
    if(!attached()) return true;
    return (m_termios.c_oflag & OPOST) != 0;
}

size_t PtyDevice::write_char_ptr(char const* text, size_t len)
{
    auto written = write(m_master_fd, text, len);
    // NOTE: We need to read back what we just wrote after writing a line
    //       when we're in echo mode
    static vector<char> tmp_readback;

    // The echo thing doesn't happen for control sequences
    // Does ECHOCTL flag alsoi control this?
    if(len == 1 && *text < 32) return written;

    if(in_echo_mode()) {
        if(text[len-1] == '\n' && has_output_processing()) written++; // the nl -> cr-nl thing
        tmp_readback.resize(written);
        int read_bytes = read(m_master_fd, tmp_readback.data(), written);
        assert(read_bytes == written);
    }

    return written;
}

void PtyDevice::write_line(LineBuffer& line)
{
    if(!attached()) return;

    if(!QUIET_MODE) fmt::print("should send {} to shell\n", line.ptr());
    write_char_ptr((char*)line.ptr(), line.size());
}

void PtyDevice::send_byte(uint8_t byte)
{
    if(!attached()) return;

    if(!QUIET_MODE) fmt::print("should send 0x{:02x} to shell\n", byte);
    write_char_ptr((char*)&byte, 1);
}

void PtyDevice::write_zero_terminated(char const* text)
{
    if(!attached()) return;

    int len = strlen(text);
    write_char_ptr(text, len);
}

void PtyDevice::send_ok_status()
{
    if(!attached()) return;
    write_char_ptr("\x1b[0n", 4);
}

void PtyDevice::send_cursor_position(glm::ivec2 pos)
{
    if(!attached()) return;
    write_zero_terminated(fmt::format("\x1b[{};{}R", pos.y, pos.x).c_str());
}
