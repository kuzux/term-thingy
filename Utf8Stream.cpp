#include "Utf8Stream.h"
#include <cassert>

// For the decode function and the table
// Copyright (c) 2008-2010 Bjoern Hoehrmann <bjoern@hoehrmann.de>
// See http://bjoern.hoehrmann.de/utf-8/decoder/dfa/ for details.

#define UTF8_ACCEPT 0
#define UTF8_REJECT 12

static const uint8_t utf8d[] = {
  // The first part of the table maps bytes to character classes that
  // to reduce the size of the transition table and create bitmasks.
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
   7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
   8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
  10,3,3,3,3,3,3,3,3,3,3,3,3,4,3,3, 11,6,6,6,5,8,8,8,8,8,8,8,8,8,8,8,

  // The second part is a transition table that maps a combination
  // of a state of the automaton and a character class to a state.
   0,12,24,36,60,96,84,12,12,12,48,72, 12,12,12,12,12,12,12,12,12,12,12,12,
  12, 0,12,12,12,12,12, 0,12, 0,12,12, 12,24,12,12,12,12,12,24,12,24,12,12,
  12,12,12,12,12,12,12,24,12,12,12,12, 12,24,12,12,12,12,12,12,12,24,12,12,
  12,12,12,12,12,12,12,36,12,36,12,12, 12,36,12,12,12,12,12,36,12,36,12,12,
  12,36,12,12,12,12,12,12,12,12,12,12, 
};

uint32_t inline
decode(uint32_t* state, uint32_t* codep, uint32_t byte) {
  uint32_t type = utf8d[byte];

  *codep = (*state != UTF8_ACCEPT) ?
    (byte & 0x3fu) | (*codep << 6) :
    (0xff >> type) & (byte);

  *state = utf8d[256 + *state + type];
  return *state;
}

Utf8Result Utf8Result::from_codepoint(char32_t codepoint)
{
    Utf8Result result;

    result.m_metadata.is_valid = true;
    result.m_data.codepoint = codepoint;
    return result;
}

Utf8Result Utf8Result::empty()
{
    Utf8Result result;

    result.m_metadata.is_valid = false;
    result.m_metadata.length = 0;

    return result;
}

Utf8Result Utf8Result::from_invalid_sequence(Utf8Stream& stream)
{
    Utf8Result result;

    result.m_metadata.is_valid = false;
    result.m_metadata.length = stream.m_buffer_position;

    for(size_t i=0; i<stream.m_buffer_position; i++) 
        result.m_data.bytes[i] = stream.m_buffer[i];
    
    stream.m_buffer_position = 0;
    return result;
}

Utf8Result Utf8Stream::feed(uint8_t byte)
{
    m_buffer[m_buffer_position++] = byte;

    decode(&m_state, &m_state_cp, byte);
    if(m_state == UTF8_REJECT) {
        m_buffer_position = 0;
        m_state = 0;
        m_state_cp = 0;
        return Utf8Result::from_invalid_sequence(*this);
    }
    if(m_state == UTF8_ACCEPT) {
        m_buffer_position = 0;
        m_state = 0;
        char32_t codepoint = m_state_cp;
        m_state_cp = 0;
        return Utf8Result::from_codepoint(codepoint);
    }

    return Utf8Result::empty();
}

bool Utf8Result::has_result() const
{
    return m_metadata.is_valid;
}

Utf8Result::Type Utf8Result::type() const
{
    if(m_metadata.is_valid) return Type::Codepoint;
    if(m_metadata.length == 0) return Type::NoResult;
    return Type::InvalidSequence;
}

char32_t Utf8Result::codepoint() const
{
    assert(m_metadata.is_valid);
    return m_data.codepoint;
}

std::vector<uint8_t> Utf8Result::invalid_bytes() const
{
    assert(!m_metadata.is_valid);
    return std::vector<uint8_t>(m_data.bytes.begin(), m_data.bytes.begin() + m_metadata.length);
}