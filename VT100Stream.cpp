#include "pch.h"
#include "VT100Stream.h"
using namespace std;

VT100Result::VT100Result(VT100Stream& stream, Type type, uint8_t byte)
    : m_stream(stream), m_type(type), m_byte(byte) { }

VT100Result::~VT100Result()
{
    if(m_type == Type::EscapeSequence || m_type == Type::InvalidEscapeSequence) {
        // TODO: Print out the unflushed buffer here
        assert(m_has_flushed && "VT100 escape code not flushed");
    }
}

size_t VT100Result::flush_buffer(vector<uint8_t>& out_buffer)
{
    size_t len = m_stream.m_buffer_position;
    for(size_t i=0; i<len; i++)
        out_buffer.push_back(m_stream.m_escape_buffer[i]);
    out_buffer.push_back(0);
    m_stream.m_buffer_position = 0;
    m_has_flushed = true;
    return len;
}

void VT100Stream::add_to_escape_buffer(uint8_t byte)
{
    assert(m_buffer_position < BufferSize);
    m_escape_buffer[m_buffer_position++] = byte;
}

VT100Result VT100Stream::feed(uint8_t byte)
{
    if(m_parse_state == ParseState::Default) {
        if(byte != 0x1b) return create_result(byte);

        add_to_escape_buffer(byte);
        m_parse_state = ParseState::SawEscape;
        return create_result();
    }
    if(m_parse_state == ParseState::SawEscape) {
        add_to_escape_buffer(byte);

        if(byte == '[') {
            m_parse_state = ParseState::SawCsi;
            return create_result();
        }

        if(byte == ']') {
            m_parse_state = ParseState::SawOsSeq;
            return create_result();
        }

        m_parse_state = ParseState::Default;
        return create_result(VT100Result::Type::InvalidEscapeSequence);
    }
    if(m_parse_state == ParseState::SawCsi) {
        add_to_escape_buffer(byte);

        if(byte == '?') {
            m_parse_state = ParseState::SawQuestionMark;
            return create_result();
        }
        if(isalpha(byte)) {
            m_parse_state = ParseState::Default;
            return create_result(VT100Result::Type::EscapeSequence);
        }

        return create_result();
    }
    if(m_parse_state == ParseState::SawQuestionMark) {
        add_to_escape_buffer(byte);

        if(isalpha(byte)) {
            m_parse_state = ParseState::Default;
            return create_result(VT100Result::Type::EscapeSequence);
        }
    }
    if(m_parse_state == ParseState::SawOsSeq) {
        add_to_escape_buffer(byte);

        if(byte == '\a') {
            m_parse_state = ParseState::Default;
            return create_result(VT100Result::Type::OperatingSystemCommand);
        }
    }

    if(m_buffer_position == BufferSize - 1)
        return create_result(VT100Result::Type::InvalidEscapeSequence);
    return create_result();
}

VT100EscapeSequence VT100Result::escape_sequence()
{
    assert(m_type == Type::EscapeSequence);

    static vector<uint8_t> tmp_buffer;
    tmp_buffer.clear();
    flush_buffer(tmp_buffer);
    return VT100EscapeSequence(tmp_buffer);
}

VT100EscapeSequence::VT100EscapeSequence(vector<uint8_t>& bytes)
{
    // assume the input is zero terminated
    assert(bytes[bytes.size()-1] == 0);

    // shortest seq is \x1b[A\x00 where A is an alphabetic character
    if(bytes.size() < 4) return;

    // setting escape seq and default values
    uint8_t specifier = bytes[bytes.size()-2];
    bool select_graphic_rendition = false;
    bool status_report_command = false;

    switch(specifier) {
        case 'H':
            m_type = Type::SetCursorPosition;
            m_arg1 = 1;
            m_arg2 = 1;
            break;
        case 'J':
            m_type = Type::EraseInScreen;
            m_arg1 = 0;
            break;
        case 'K':
            m_type = Type::EraseInLine;
            m_arg1 = 0;
            break;
        case 'L':
            m_type = Type::InsertLines;
            m_arg1 = 1;
            break;
        case 'M':
            m_type = Type::DeleteLines;
            m_arg1 = 1;
            break;
        case 'm':
            select_graphic_rendition = true; // type will be determined by the arg
            m_arg1 = 0;
            break;
        case 'A':
            m_type = Type::MoveCursorUp;
            m_arg1 = 1;
            break;
        case 'B':
            m_type = Type::MoveCursorDown;
            m_arg1 = 1;
            break;
        case 'C':
            m_type = Type::MoveCursorRight;
            m_arg1 = 1;
            break;
        case 'D':
            m_type = Type::MoveCursorLeft;
            m_arg1 = 1;
            break;
        case 'X':
            m_type = Type::EraseCharacters;
            m_arg1 = 1;
            break;
        case 'P':
            m_type = Type::DeleteCharacters;
            m_arg1 = 1;
            break;
        case 'n':
            status_report_command = true;
            // type will be determined by the arg
            break;
        default:
            fmt::print("Unrecognized escape sequence \\x1b{}\n", reinterpret_cast<const char*>(bytes.data()+1));
            return;
    }

    int semicolon_idx = -1;
    for(size_t i=0; i<bytes.size(); i++) {
        if(bytes[i] == ';') {
            semicolon_idx = i;
            break;
        }
    }

    bool parsed_two_args = false;

    if(semicolon_idx >= 0) {
        bytes[semicolon_idx] = 0;
        parsed_two_args = true;

        int tmp = 0;
        char* end = (char*)bytes.data()+2;
        tmp = strtol((const char*)bytes.data()+2, &end, 10);
        if(end != (char*)bytes.data()+2) m_arg1 = tmp;

        tmp = 0;
        end = (char*)bytes.data()+semicolon_idx;
        tmp = strtol((const char*)bytes.data()+semicolon_idx+1, &end, 10);
        if(end != (char*)bytes.data()+semicolon_idx) m_arg2 = tmp;
    } else {
        // making sure the numeric string is zero-terminated
        bytes[bytes.size()-2] = 0;

        char* end = (char*)bytes.data()+2;
        int tmp = strtol((const char*)bytes.data()+2, &end, 10);
        if(end != (char*)bytes.data()+2) m_arg1 = tmp;
    }

    if(select_graphic_rendition) {
        if(m_arg1 == 0) {
            m_type = Type::ResetGraphicsMode;
            return;
        }

        int color_arg = parsed_two_args ? m_arg2 : m_arg1;
        bool is_bright = parsed_two_args && (m_arg1 == 1);

        if(color_arg >= 30 && color_arg <= 37) {
            m_type = Type::SetForegroundColor;
            m_arg1 = color_arg - 30;
        }
        if(color_arg >= 40 && color_arg <= 47) {
            m_type = Type::SetBackgroundColor;
            m_arg1 = color_arg - 40;
        }
        if(color_arg >= 90 && color_arg <= 97) {
            m_type = Type::SetForegroundColor;
            m_arg1 = color_arg - 90 + 8;
        }
        if(color_arg >= 100 && color_arg <= 107) {
            m_type = Type::SetBackgroundColor;
            m_arg1 = color_arg - 100 + 8;
        }

        if(parsed_two_args && is_bright)
            m_arg1 += 8;
    }

    if(status_report_command) {
        if(m_arg1 == 5) m_type = Type::DeviceStatusReport;
        else if(m_arg1 == 6) m_type = Type::CursorPositionReport;
        else m_type = Type::Unknown;
    }

    // TODO: Support OS sequences and changing window title via an os sequence
}
