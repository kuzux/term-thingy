#pragma once

#include "pch.h"
#include <fontconfig/fontconfig.h>

class FontLoader
{
public:
    FontLoader();
    ~FontLoader();

    std::optional<std::string> locate_font(std::string const& pattern);
private:
    FcConfig* m_config { nullptr };
};