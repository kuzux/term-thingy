#include "pch.h"
#include "MouseSelection.h"

#include "vector_formatter.h"

using namespace std;

MouseSelection::MouseSelection(glm::ivec2 window_size, vector<PerCharacterInfo> const& characters_on_screen)
    : m_window_size_in_px(window_size), m_characters_on_screen(characters_on_screen) { }

void MouseSelection::start(glm::ivec2 pixel_position)
{
    m_mouse_is_down = true;
    auto character_index = character_index_under_mouse(pixel_position);
    if(!character_index) return;

    m_selection_range.start = *character_index;
    m_selection_range.end = *character_index;

    // TODO: Handle double click selects word
}

void MouseSelection::update(glm::ivec2 pixel_position)
{
    if(!m_mouse_is_down) return;

    auto character_index = character_index_under_mouse(pixel_position);
    if(!character_index) return;

    m_selection_range.end = *character_index;
}

void MouseSelection::end(glm::ivec2 pixel_position)
{
    m_mouse_is_down = false;
    auto character_index = character_index_under_mouse(pixel_position);

    m_selection_range.end = *character_index;
}

glm::vec2 MouseSelection::mouse_position_to_gl(glm::ivec2 pixel_position) const {
    // mapping to (0, 1) first
    glm::vec2 window_uv = glm::vec2(pixel_position) / glm::vec2(m_window_size_in_px);
    return { -1.f + 2*window_uv.x, 1.f - 2*window_uv.y };
}

// mouse pos is in pixels relative to the window
optional<size_t> MouseSelection::character_index_under_mouse(glm::ivec2 pixel_position) const
{
    auto mouse_pos = mouse_position_to_gl(pixel_position);
    // What can be done for handling non-monospaced font and RTL text rendering:
    //     1. A copy of the characters is stored for mouse position calculation
    //        It is modified every time the screen buffer changes (re-sorted)
    //        The copy only includes the relevant fields and is sorted by screen position
    //        We then do a binary search on this
    //        This optimizes for mouse click happening more often than buffer change
    //     2. We just do a linear search of characters
    //        This optimizes for buffer change happening more often than mouse click (sounds more likely :D)
    //     3. Like option 1 but try to optimize for character insertion case
    //        This fails when loads of characters get inserted at the same time (when reading from pty)
    //     4. Lazily populate an option 1 sorted thing when the function gets called
    //        Invalidate it when characters are changed, but don't regenerate
    //        So that we get more efficient search on click and drag, but don't do it
    //        everytime we read data from the pty (Too much work, kind of)

    int idx = -1;
    for(size_t i=0; i<m_characters_on_screen.size(); i++) {
        auto& info = m_characters_on_screen[i];

        // TODO: Don't assume left-to right, top-to-bottom here
        assert(info.bg_draw_size.x >= 0);
        assert(info.bg_draw_size.y <= 0);

        if(info.bg_draw_pos.y < mouse_pos.y) continue;
        if(info.bg_draw_pos.y + info.bg_draw_size.y > mouse_pos.y) continue;

        if(info.bg_draw_pos.x > mouse_pos.x) continue;
        if(info.bg_draw_pos.x + info.bg_draw_size.x < mouse_pos.x) continue;

        idx = i;
        break;
    }

    if(idx == -1) return {};

    // are we on left half or right half or right half of the character, horizontally
    // (the "half" might need to be adjusted though)
    auto& info = m_characters_on_screen[idx];
    // if we're on the right half, we select the next character
    // (so that selections from/to the edge of the window work)
    if(mouse_pos.x > (info.bg_draw_pos.x + info.bg_draw_size.x/2))
        idx++;

    return idx;
}
