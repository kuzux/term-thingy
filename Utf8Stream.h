#pragma once
#include "pch.h"
#include "Error.h"

class Utf8Stream;
constexpr size_t MaxBytesPerUtf8Char = 5;

class Utf8Result
{
    friend class Utf8Stream;
public:
    enum class Type{
        Codepoint,
        NoResult,
        InvalidSequence
    };

    bool has_result() const;
    Type type() const;
    char32_t codepoint() const;
    // TODO: This should be a std::span. Convert after switching to c++20
    std::vector<uint8_t> invalid_bytes() const;
private:
    static Utf8Result from_codepoint(char32_t codepoint);
    static Utf8Result empty();
    static Utf8Result from_invalid_sequence(Utf8Stream& stream);

    union
    {
        char32_t codepoint;
        std::array<uint8_t, MaxBytesPerUtf8Char> bytes;
    } m_data;
    struct
    {
        bool is_valid : 1;

        // Length is defined for invalid sequences only
        uint8_t length: 3; // Since length is at most 5, 3 bits are enough
    } m_metadata;
};

class Utf8Stream
{
    friend class Utf8Result;
public:
    Utf8Stream() { }
    ~Utf8Stream() { }
    [[nodiscard]] Utf8Result feed(uint8_t byte);
private:
    size_t m_buffer_position { 0 };
    uint32_t m_state { 0 };
    uint32_t m_state_cp { 0 };
    std::array<uint8_t, MaxBytesPerUtf8Char> m_buffer;
};
