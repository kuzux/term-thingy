#pragma once
#include "pch.h"

class GLVertexArray
{
public:
    struct Attribute {
        int32_t num_components;
        GLenum gl_type;
        size_t stride;
        size_t offset;

        size_t divisor;
    };

    GLVertexArray();
    GLVertexArray(GLVertexArray&& o);
    ~GLVertexArray();

    void bind(std::function<void()> body);

    // TODO: This should be fallible
    void define_attribute(uint32_t index, Attribute attr);
private:

    GLuint m_glid { 0 };
    bool m_is_bound { false };
    std::unordered_set<uint32_t> m_enabled_attrs;
};

// Is there a better way to do a compile time switch case based on type?
inline int32_t __num_components_gl_attribute(const char* class_name)
{
    if(!strcmp(class_name, "float")) return 1;
    if(!strcmp(class_name, "glm::vec2")) return 2;
    if(!strcmp(class_name, "glm::vec3")) return 3;
    if(!strcmp(class_name, "glm::vec4")) return 4;

    if(!strcmp(class_name, "int")) return 1;
    if(!strcmp(class_name, "glm::ivec2")) return 2;
    if(!strcmp(class_name, "glm::ivec3")) return 3;
    if(!strcmp(class_name, "glm::ivec4")) return 4;

    assert(false && "Unknown gl attribute type");
}

inline GLenum __gl_type_gl_attribute(const char* class_name)
{
    if(!strcmp(class_name, "float")) return GL_FLOAT;
    if(!strcmp(class_name, "glm::vec2")) return GL_FLOAT;
    if(!strcmp(class_name, "glm::vec3")) return GL_FLOAT;
    if(!strcmp(class_name, "glm::vec4")) return GL_FLOAT;

    if(!strcmp(class_name, "int")) return GL_INT;
    if(!strcmp(class_name, "glm::ivec2")) return GL_INT;
    if(!strcmp(class_name, "glm::ivec3")) return GL_INT;
    if(!strcmp(class_name, "glm::ivec4")) return GL_INT;

    assert(false && "Unknown gl attribute type");
}

// TODO: Support integral & matrix types as well
#define __GLVertexAttribute_Impl(type_name, struct_name, field_name, divisor) \
    GLVertexArray::Attribute { \
        __num_components_gl_attribute(#type_name), \
        __gl_type_gl_attribute(#type_name), \
        sizeof(struct_name), \
        offsetof(struct_name, field_name), \
        divisor \
    }
#define GLVertexAttribute(type_name, struct_name, field_name) __GLVertexAttribute_Impl(type_name, struct_name, field_name, 0)
#define GLInstancedAttribute(type_name, struct_name, field_name) __GLVertexAttribute_Impl(type_name, struct_name, field_name, 1)
