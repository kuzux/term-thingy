#pragma once
#include "pch.h"
#include "FontAtlas.h"
#include "Range.h"

struct CharInBuffer
{
    char32_t codepoint;
    uint8_t fg_color;
    uint8_t bg_color;
};

class ScrollbackBuffer
{
public:
    ScrollbackBuffer(size_t max_rows, size_t width)
        : m_width(width), m_max_rows(max_rows) { }
    ~ScrollbackBuffer() = default;

    void set_max_rows(size_t max_rows) { m_max_rows = max_rows; }
    size_t max_rows() const { return m_max_rows; }

    void resize_to_width(size_t width) { m_width = width; }
    size_t width() const { return m_width; }
    void insert_line(std::vector<CharInBuffer>::iterator begin, std::vector<CharInBuffer>::iterator end);
    CharInBuffer at(size_t line, size_t column) const;

    bool dirty() const { return m_dirty; }
    void clean() { m_dirty = false; }

private:
    using Line = std::vector<CharInBuffer>;
    std::deque<Line> m_lines;

    size_t m_width;
    size_t m_max_rows;

    bool m_dirty { false };
};

class ScreenBuffer
{
public:
    ScreenBuffer(ScrollbackBuffer& scrollback_buffer, size_t rows, size_t columns);
    ~ScreenBuffer() = default;

    glm::ivec2 size_in_chars() const { return m_size; }
    void resize(glm::ivec2 new_size);

    bool dirty() const { return m_dirty; }
    // TODO: clean() should probably take a Badge<BufferDisplay>
    void clean() { m_dirty = false; }

    void insert_char(char32_t ch);
    void backspace();
    void newline();
    void carriage_return();
    void tab();
    void delete_characters(size_t n);

    bool should_beep() const { return m_should_beep; }
    void mark_as_beeped() { m_should_beep = false; }

    glm::ivec2 cursor_position_1_indexed() const { return m_cursor + glm::ivec2(1); }
    int cursor_index() const { return m_cursor.y*m_size.x + m_cursor.x; }

    // TODO: Optionally take counts here
    void move_up();
    void move_down();
    void move_left();
    void move_right();

    void set_cursor_position(glm::ivec2 cursor);
    void clear_to_beginning();
    void clear_to_end();
    void clear_screen();
    void clear_line_to_beginning();
    void clear_line_to_end();
    void clear_line();
    void erase_characters(size_t n);

    void insert_empty_lines(size_t n);
    void delete_lines(size_t n);

    void set_foreground_color(uint8_t color);
    void set_background_color(uint8_t color);
    void reset_rendition_params();

    void set_selection(Range<size_t> range);
    std::string get_text_in_range(Range<size_t> range);

    std::vector<CharInBuffer> const& characters() const { return m_characters; }
    Range<size_t> selection() const { return m_selection; }

    CharInBuffer at(size_t line, size_t column) const;

private:
    ScrollbackBuffer& m_scrollback_buffer;
    std::vector<CharInBuffer> m_characters;

    // (x, y) is (cols, rows)
    glm::ivec2 m_size;
    glm::ivec2 m_cursor { 0, 0 };
    uint8_t m_current_fg_color { 15 };
    uint8_t m_current_bg_color { 0 };
    Range<size_t> m_selection { 0, 0 };

    bool m_should_beep { false };
    bool m_dirty { true }; // need to sync gl buffer once
};

class LineBuffer {
public:
    size_t size() const { return m_size; }
    const char* ptr() { m_data[m_size] = 0; return reinterpret_cast<const char*>(m_data.data()); }

    void insert_zero_terminated(char const* bytes)
    {
        size_t len = strlen(bytes);
        assert(m_size + len <= m_data.size() - 1);
        for(size_t i=0; i<len; i++)
            m_data[m_size++] = (uint8_t)bytes[i];
    }
    void backspace() { if(m_size) m_size--; }

    void clear() { m_size = 0; }
private:
    std::array<uint8_t, 255> m_data;
    size_t m_size { 0 };
    // TODO: We need a cursor here as well
};
