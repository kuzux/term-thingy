#include "pch.h"
#include "ScreenBuffer.h"
using namespace std;

void ScrollbackBuffer::insert_line(vector<CharInBuffer>::iterator begin, vector<CharInBuffer>::iterator end)
{
    if(m_lines.size() == m_max_rows) m_lines.pop_back();

    m_lines.push_front(Line(begin, end));
    m_dirty = true;
}

CharInBuffer ScrollbackBuffer::at(size_t line, size_t column) const
{
    if(line >= m_lines.size()) return {' ', 15, 0};
    if(column >= m_lines[line].size()) return {' ', 15, 0};
    return m_lines[line][column];
}

ScreenBuffer::ScreenBuffer(ScrollbackBuffer& scrollback_buffer, size_t rows, size_t columns)
    : m_scrollback_buffer(scrollback_buffer), m_size(columns, rows)
{
    m_characters.reserve(rows*columns);
    for(size_t i=0; i<rows*columns; i++) m_characters.push_back({' ', m_current_fg_color, m_current_bg_color});
}

void ScreenBuffer::insert_char(char32_t ch)
{
    if(ch < 32) {
        if(ch == '\a') m_should_beep = true;
        else if(ch == '\b') backspace();
        else if(ch == '\n') newline();
        else if(ch == '\r') carriage_return();
        else if(ch == '\t') tab();
        else insert_char(0x25af); // ▯
        return;
    }

    if(m_cursor.x == m_size.x) return;

    size_t idx = m_cursor.y * m_size.x + m_cursor.x;
    m_characters[idx].codepoint = ch;
    m_characters[idx].fg_color = m_current_fg_color;
    m_characters[idx].bg_color = m_current_bg_color;
    m_cursor.x++;
    m_dirty = true;
}

void ScreenBuffer::backspace()
{
    // TODO: Handle deleting last char of the line
    //       What do we want to do in this case?
    if(m_cursor.x == 0) return;

    size_t idx = m_cursor.y * m_size.x + m_cursor.x - 1;
    m_characters[idx].codepoint = ' ';
    m_cursor.x--;
    m_dirty = true;
}

void ScreenBuffer::newline()
{
    m_dirty = true;

    if(m_cursor.y == m_size.y - 1) {
        m_scrollback_buffer.insert_line(m_characters.begin(), m_characters.begin()+m_size.x);
        m_characters.erase(m_characters.begin(), m_characters.begin()+m_size.x);
        for(int i=0; i<m_size.x; i++) {
            m_characters.push_back({' ', m_current_fg_color, m_current_bg_color});
        }
        m_cursor.x = 0;
        return;
    }

    m_cursor.x = 0;
    m_cursor.y++;
}

void ScreenBuffer::carriage_return()
{
    m_dirty = true;
    m_cursor.x = 0;
}

void ScreenBuffer::tab()
{
    m_dirty = true;
    do {
        m_cursor.x++;
    } while(m_cursor.x % 8 != 0);
}

void ScreenBuffer::move_up()
{
    if(m_cursor.y == 0) return;
    m_cursor.y--;
    m_dirty = true;
}

void ScreenBuffer::move_down()
{
    if(m_cursor.y == m_size.y-1) return;
    m_cursor.y++;
    m_dirty = true;
}

void ScreenBuffer::move_left()
{
    if(m_cursor.x == 0) return;
    m_cursor.x--;
    m_dirty = true;
}

void ScreenBuffer::move_right()
{
    if(m_cursor.x == m_size.x-1) return;
    m_cursor.x++;
    m_dirty = true;
}

void ScreenBuffer::set_cursor_position(glm::ivec2 cursor)
{
    if(cursor.x < 0) cursor.x = 0;
    if(cursor.y < 0) cursor.y = 0;
    if(cursor.x >= m_size.x) cursor.x = m_size.x - 1;
    if(cursor.y >= m_size.y) cursor.y = m_size.y - 1;

    m_cursor = cursor;
    m_dirty = true;
}

void ScreenBuffer::clear_to_beginning()
{
    size_t idx = cursor_index();
    for(size_t i=0; i<idx; i++) m_characters[i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::clear_to_end()
{
    size_t idx = cursor_index();
    for(size_t i=idx; i<m_characters.size(); i++) m_characters[i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::clear_screen()
{
    for(size_t i=0; i<m_characters.size(); i++) m_characters[i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::clear_line_to_beginning()
{
    size_t idx = cursor_index();
    size_t start_of_line = m_cursor.y * m_size.x;
    for(size_t i=start_of_line; i<idx; i++) m_characters[i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::clear_line_to_end()
{
    size_t idx = cursor_index();
    size_t end_of_line = (m_cursor.y+1) * m_size.x;
    for(size_t i=idx; i<end_of_line; i++) m_characters[i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::clear_line()
{
    size_t start_of_line = m_cursor.y * m_size.x;
    size_t end_of_line = (m_cursor.y+1) * m_size.x;
    for(size_t i=start_of_line; i<end_of_line; i++) m_characters[i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::erase_characters(size_t n)
{
    size_t idx = cursor_index();
    for(size_t i=0; i<n; i++) m_characters[idx+i] = {' ', m_current_fg_color, m_current_bg_color};
    m_dirty = true;
}

void ScreenBuffer::delete_characters(size_t n)
{
    size_t idx = cursor_index();

    for(size_t i=n; i>0; i--) {
        m_characters[idx+i] = m_characters[idx+n+i];
        m_characters[idx+n+i] = {' ', m_current_fg_color, m_current_bg_color};
    }
    m_dirty = true;
}

void ScreenBuffer::set_foreground_color(uint8_t color)
{
    assert(color < 16);
    m_current_fg_color = color;
    m_dirty = true;
}

void ScreenBuffer::set_background_color(uint8_t color)
{
    assert(color < 16);
    m_current_bg_color = color;
    m_dirty = true;
}

void ScreenBuffer::reset_rendition_params()
{
    m_current_bg_color = 0;
    m_current_fg_color = 15;
    m_dirty = true;
}

void ScreenBuffer::set_selection(Range<size_t> range)
{
    if(m_selection == range) return;

    m_selection = range;
    m_dirty = true;
}

void ScreenBuffer::insert_empty_lines(size_t n)
{
    m_characters.insert(m_characters.begin() + cursor_index(), n*m_size.x, {' ', m_current_fg_color, m_current_bg_color});
    m_characters.resize(m_size.x * m_size.y);
    m_dirty = true;
}

void ScreenBuffer::delete_lines(size_t n)
{
    m_characters.erase(m_characters.begin() + cursor_index(), m_characters.begin() + cursor_index() + n*m_size.x);
    
    size_t current_length = m_characters.size();
    size_t target_length = m_size.x * m_size.y;
    for(size_t i=current_length; i<target_length; i++)
        m_characters.push_back({' ', m_current_fg_color, m_current_bg_color});

    m_dirty = true;
}

std::string ScreenBuffer::get_text_in_range(Range<size_t> range)
{
    string result;
    for(size_t i=range.start; i<range.end; i++) {
        // adding unicode codepoints to string will result in utf-8 encoding
        result.push_back(m_characters[i].codepoint);
        if(i%m_size.x == m_size.x-1) result.push_back('\n');
    }
    return result;
}

void ScreenBuffer::resize(glm::ivec2 new_size)
{
    vector<CharInBuffer> new_characters(new_size.x * new_size.y, CharInBuffer{' ', m_current_fg_color, m_current_bg_color});

    glm::ivec2 copy_size { min(m_size.x, new_size.x), min(m_size.y, new_size.y) };
    for(int i=0; i<copy_size.y; i++) {
        for(int j=0; j<copy_size.x; j++) {
            int old_idx = i*m_size.x + j;
            int new_idx = i*new_size.x + j;

            new_characters[new_idx] = m_characters[old_idx];
        }
    }

    m_size = new_size;
    if(m_cursor.x >= m_size.x) m_cursor.x = m_size.x - 1;
    if(m_cursor.y >= m_size.y) m_cursor.y = m_size.y - 1;
    m_characters.swap(new_characters);
    m_dirty = true;
}

CharInBuffer ScreenBuffer::at(size_t line, size_t column) const
{
    if((int)line >= m_size.y) return {' ', 15, 0};
    if((int)column >= m_size.x) return {' ', 15, 0};
    return m_characters[line*m_size.x + column];
}
