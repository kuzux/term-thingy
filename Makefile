RELEASE?=0

PROG=term-thingy
OSX_PACKAGE=Term-Thingy.app
SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:.cpp=.o)
DEPFILES=$(SRCS:.cpp=.d)
PKGS=sdl2 fmt glm glew freetype2 CLI11 fontconfig
LINUXDEPLOY=~/opt/linuxdeploy-x86_64.AppImage
APPIMAGETOOL=~/opt/appimagetool-x86_64.AppImage

CXXFLAGS=-fsanitize=address -Wall -Wextra -Wunused-variable -std=c++17 -MMD
LDFLAGS=-fsanitize=address

ifeq ($(shell uname), Linux)
	LDFLAGS+=-static-libasan
	PKGS+=libdw
	CXXFLAGS+=-DBACKWARD_HAS_DW=1
else
	PKGS+=libdwarf
	CXXFLAGS+=-DBACKWARD_HAS_DWARF=1
endif

ifeq ($(shell uname), Darwin)
	OBJS+=platform_osx.o
else
	OBJS+=platform_linux.o
endif

ifeq ($(shell uname), Darwin)
	LDFLAGS+=-framework AppKit
endif

ifeq ($(shell pkg-config opengl; echo $$?),0)
	PKGS+=opengl
	CXXFLAGS+=-DGL_HEADER_FILE='<GL/gl.h>'
else ifneq ($(shell uname), Darwin)
	$(error "No opengl package found")
else
	CXXFLAGS+=-DGL_HEADER_FILE='<OpenGL/gl.h>'
	LDFLAGS+=-framework OpenGL
endif

ifeq ($(shell pkg-config opengl; echo $$?),0)
	PKGS+=libunwind
	CXXFLAGS+=-DBACKWARD_HAS_LIBUNWIND=1
endif

CXXFLAGS+=$(shell pkg-config --cflags $(PKGS))
LDFLAGS+=$(shell pkg-config --libs $(PKGS))

ifeq ($(RELEASE), 1)
	CXXFLAGS+=-O2
else
	CXXFLAGS+=-g
endif

all: $(PROG)

$(PROG): pch.h.gch $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS)

pch.h.gch: pch.h
	$(CXX) -x c++-header -c $< -o $@ $(CXXFLAGS)

platform_osx.o: platform/platform_osx.m
	clang -x objective-c -c $< -o $@

platform_linux.o: platform/platform_linux.c
	$(CC) -c $< -o $@ $(CXXFLAGS)

%.o : %.cpp
	$(CXX) -include pch.h -c $< -o $@ $(CXXFLAGS)

osx-package: $(PROG) assets/Info.plist
	mkdir -p $(OSX_PACKAGE)/Contents/MacOS
	cp $(PROG) $(OSX_PACKAGE)/Contents/MacOS
	cp -r assets $(OSX_PACKAGE)/Contents/Resources
# TODO: Generate thhe Info.plist file with a script
	cp assets/Info.plist $(OSX_PACKAGE)/Contents
# This unholy thing copies every homebrew dependency into the app bundle
	mkdir -p $(OSX_PACKAGE)/Contents/Frameworks
	otool -L $(PROG) | grep homebrew | cut -w -f2 - | xargs -I _ cp -f _ $(OSX_PACKAGE)/Contents/Frameworks
	codesign -s - $(OSX_PACKAGE)

create-dmg: osx-package dmgbuild.json
	dmgbuild -s dmgbuild.json "Term-Thingy" $(OSX_PACKAGE).dmg

create-appimage: term-thingy
	rm -rf $(PROG).AppDir
	$(LINUXDEPLOY) -e $(PROG) --appdir=$(PROG).AppDir -i assets/logo.svg -d assets/term-thingy.desktop
	cp -r assets $(PROG).AppDir/usr/share
	$(APPIMAGETOOL) $(PROG).AppDir $(PROG).AppImage

clean:
	rm -f $(PROG) $(OBJS) $(DEPFILES) pch.h.gch
	rm -rf $(OSX_PACKAGE)
	rm -f $(OSX_PACKAGE).dmg
	rm -rf $(PROG).AppDir
	rm -f $(PROG).AppImage

-include ${DEPFILES}
