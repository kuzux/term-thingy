#include "pch.h"
#include "BufferDisplay.h"

using namespace std;

BufferDisplay::BufferDisplay(ScreenBuffer& screen_buffer, ScrollbackBuffer& scrollback_buffer,
    FontAtlas& atlas, float scale) :
    m_screen_buffer(screen_buffer), m_scrollback_buffer(scrollback_buffer), 
    m_atlas(atlas), m_scale(scale, scale)
{ }

bool BufferDisplay::dirty() const
{
    return m_screen_buffer.dirty() || m_scrollback_buffer.dirty() || m_dirty;
}

static constexpr array<glm::vec3, 16> colors = {{
    { 0.f,  0.f,  0.f  },
    { 0.8f, 0.f,  0.f  },
    { 0.f , 0.8f, 0.f  },
    { 0.8f, 0.8f, 0.f  },
    { 0.f,  0.f,  0.9f },
    { 0.8f, 0.f,  0.8f },
    { 0.f , 0.8f, 0.8f },
    { 0.8f, 0.8f, 0.8f },

    { 0.5f, 0.5f, 0.5f },
    { 1.f,  0.f,  0.f  },
    { 0.f,  1.f,  0.f  },
    { 1.f,  1.f,  0.f  },
    { 0.f,  0.f,  1.f  },
    { 1.f,  0.f,  1.f  },
    { 0.f,  1.f,  1.f  },
    { 1.f,  1.f,  1.f  }
}};

void BufferDisplay::regenerate_character_info(vector<PerCharacterInfo>& buffer) {
    buffer.clear();

    auto size = m_screen_buffer.size_in_chars();

    // top left of the logical quad
    glm::vec2 top_left = { -1.0, 1.0 };

    // direction from top left to center
    glm::vec2 scaled_center_direction { m_scale.x, -m_scale.y };

    unordered_set<char32_t> cps_to_render;
    for(auto const& ch : m_screen_buffer.characters()) {
        if(!m_atlas.glyph_in_cache(ch.codepoint))
            cps_to_render.insert(ch.codepoint);
    }
    for(int i=0; i < size.y*size.x; i++) {
    }
    m_atlas.generate_glyphs(cps_to_render);

    // all character heights are the same
    float advance_y = m_scale.y * m_atlas.get_metrics_for_codepoint('M').advance.y;

    auto add_character = [&](size_t i, size_t j, CharInBuffer ch){
        auto idx = i*size.x + j;
        int cp = ch.codepoint;
        auto metrics = m_atlas.get_metrics_for_codepoint(cp);

        int fg_color = ch.fg_color;
        int bg_color = ch.bg_color;

        if(m_screen_buffer.selection().contains(idx)) swap(fg_color, bg_color);

        // top left of the textured quad
        auto quad_coord = top_left + scaled_center_direction * metrics.bitmap_offset;
        auto quad_size = m_scale * metrics.bitmap_size;
        quad_coord += quad_size * glm::vec2(0., -1.);

        // we need to pass the top left of the character
        buffer.push_back({
            quad_coord,
            quad_size,

            metrics.tex_coords,
            metrics.tex_size,

            colors[fg_color],
            colors[bg_color],

            top_left,
            metrics.advance * scaled_center_direction
        });

        top_left.x += m_scale.x * metrics.advance.x;
        if((int)j == size.x-1) {
            top_left.x = -1.0;
            top_left.y -= advance_y;
        }
    };

    for(int i=0; i<size.y; i++) {
        bool from_scrollback = (i < m_scroll_offset);

        for(int j=0; j<size.x; j++) {
            CharInBuffer ch;
            if(from_scrollback) ch = m_scrollback_buffer.at(m_scroll_offset - i, j);
            else ch = m_screen_buffer.at(i - m_scroll_offset, j);
            add_character(i, j, ch);
        }
    }

    m_screen_buffer.clean();
    m_scrollback_buffer.clean();
    m_dirty = false;
}

void BufferDisplay::scale_by(glm::vec2 scale)
{
    m_scale *= scale;
    m_dirty = true;
}

void BufferDisplay::scroll_up()
{
    // TODO: Add a check so we can't scroll past some max amount
    m_scroll_offset++;
    fmt::print("scroll up, new offset {}\n", m_scroll_offset);
    m_dirty = true;
}

void BufferDisplay::scroll_down()
{
    if(m_scroll_offset == 0) return;
    m_scroll_offset--;
    fmt::print("scroll down, new offset {}\n", m_scroll_offset);
    m_dirty = true;
}
