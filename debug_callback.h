#pragma once
#include "pch.h"

void glDebugCallback(GLenum source, GLenum type, GLuint id,
                     GLenum severity, GLsizei length,
                     const GLchar *msg, const void *data);