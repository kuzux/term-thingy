#pragma once
#include "pch.h"

struct PerVertexInfo {
    glm::vec3 vertex_position;
    glm::vec2 texture_coords;
};

struct PerCharacterInfo {
    glm::vec2 draw_pos;
    glm::vec2 draw_size;

    glm::vec2 font_pos;
    glm::vec2 font_size;

    glm::vec3 fg_color;
    glm::vec3 bg_color;

    // similar to the original draw pos but without the texture offset
    glm::vec2 bg_draw_pos;
    glm::vec2 bg_draw_size;
};
