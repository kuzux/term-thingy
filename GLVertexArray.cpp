#include "pch.h"
#include "GLVertexArray.h"
using namespace std;

GLVertexArray::GLVertexArray()
{
    glGenVertexArrays(1, &m_glid);
}

GLVertexArray::GLVertexArray(GLVertexArray&& o) 
    : m_glid(o.m_glid)
    , m_enabled_attrs(move(o.m_enabled_attrs))
    {
        assert(!o.m_is_bound);
        o.m_glid = 0;
    }

GLVertexArray::~GLVertexArray()
{
    assert(!m_is_bound);

    if(m_glid) {
        glDeleteVertexArrays(1, &m_glid);
    }
}

void GLVertexArray::bind(function<void()> body)
{
    assert(m_glid);

    // Make sure no other vao is bound
    GLint currently_bound_vao;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &currently_bound_vao);
    assert(!currently_bound_vao);

    m_is_bound = true;
    glBindVertexArray(m_glid);
    body();
    glBindVertexArray(0);
    m_is_bound = false;
}

void GLVertexArray::define_attribute(uint32_t index, Attribute attr)
{
    assert(m_is_bound);
    bool attr_exists = m_enabled_attrs.find(index) != m_enabled_attrs.end();
    assert(!attr_exists && "Trying to enable attr with same index");

    GLint currently_bound_vao;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &currently_bound_vao);
    assert(currently_bound_vao == (GLint)m_glid && "Trying to define an attribute but the vao is not bound");

    m_enabled_attrs.insert(index);
    glEnableVertexAttribArray(index);
    if(attr.gl_type == GL_INT)
        glVertexAttribIPointer(index, attr.num_components, attr.gl_type, attr.stride, (void*)attr.offset);
    else
        glVertexAttribPointer(index, attr.num_components, attr.gl_type, GL_FALSE, attr.stride, (void*)attr.offset);

    if(attr.divisor)
        glVertexAttribDivisor(index, attr.divisor);
}
