#include <GL/glew.h>

#define GL_GLEXT_PROTOTYPES 1
#define GL_SILENCE_DEPRECATION // make osx complain less
#include GL_HEADER_FILE

#include "vendor/stb_image.h"

#include <stdlib.h>
#include <assert.h>

#include <fmt/core.h>

#include <fstream>
#include <sstream>
#include <array>
#include <vector>
#include <deque>
#include <string_view>
#include <optional>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <glm/glm.hpp>

#include <CLI/CLI.hpp>
