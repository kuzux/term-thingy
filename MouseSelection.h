#pragma once
#include "pch.h"
#include "Range.h"
#include "gl_buffer_defs.h"

class MouseSelection
{
public:
    MouseSelection(glm::ivec2 window_size_in_px, std::vector<PerCharacterInfo> const& characters_on_screen);
    ~MouseSelection() { }
    inline bool contains(int idx) const { return m_selection_range.contains(idx); }

    inline void set_window_size(glm::ivec2 window_size_in_px) { m_window_size_in_px = window_size_in_px; }

    void start(glm::ivec2 pixel_position);
    void update(glm::ivec2 pixel_position);
    void end(glm::ivec2 pixel_position);

    inline Range<size_t> range() const { return m_selection_range; }
private:
    std::optional<size_t> character_index_under_mouse(glm::ivec2 pixel_position) const;
    glm::vec2 mouse_position_to_gl(glm::ivec2 pixel_position) const;

    glm::ivec2 m_window_size_in_px;
    std::vector<PerCharacterInfo> const& m_characters_on_screen;

    Range<size_t> m_selection_range { 0, 0 };
    bool m_mouse_is_down { false };
};