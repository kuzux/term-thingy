#include "pch.h"
#include "debug_callback.h"
#include "vector_formatter.h"
#include "gl_buffer_defs.h"
#include "Error.h"
#include "SDLWindow.h"
#include "GLVertexArray.h"
#include "Utf8Stream.h"
#include "FontAtlas.h"
#include "VT100Stream.h"
#include "PtyDevice.h"
#include "ScreenBuffer.h"
#include "BufferDisplay.h"
#include "FontLoader.h"
#include "Range.h"
#include "MouseSelection.h"
#include "platform/platform.h"

#include "vendor/stb_image.h"
#include <SDL.h>

#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

using namespace std;

bool QUIET_MODE = false;

static constexpr array<PerVertexInfo, 6> vertex_buffer_data = {{
    // vertex position      texture coords
    {{ 0.f, 0.f, 0.f },   { 0.f, 1.f }},
    {{ 1.f, 0.f, 0.f },   { 1.f, 1.f }},
    {{ 0.f, 1.f, 0.f },   { 0.f, 0.f }},

    {{ 0.f, 1.f, 0.f },   { 0.f, 0.f }},
    {{ 1.f, 0.f, 0.f },   { 1.f, 1.f }},
    {{ 1.f, 1.f, 0.f },   { 1.f, 0.f }}
}};

vector<PerCharacterInfo> instanced_info;

template<typename Fn>
class Defer {
public:
    Defer(Fn fn) : m_fn(fn) { }
    ~Defer() { if(m_fn) (*m_fn)(); }
    Defer(const Defer&) = delete;
    Defer(Defer&& o) : m_fn(o.m_fn) { o.m_fn = {}; }
private:
    optional<Fn> m_fn;
};

class GLShader
{
public:
    enum class Type
    {
        Vertex, Fragment
    };

    static ErrorOr<GLShader> from_file(string const& filename, Type type);
    ~GLShader();

    GLuint gl_id() const;

    GLShader(GLShader&& shader);
private:
    GLShader(string_view source, Type type);
    bool compile();
    string error_message();

    Type m_type;
    string m_source;
    GLuint m_glid { 0 };
    bool m_is_compiled { false };
};

ErrorOr<GLShader> GLShader::from_file(string const& filename, Type type)
{
    ifstream source_file(filename);
    stringstream sst;
    sst << source_file.rdbuf();
    GLShader res { sst.str(), type };

    // This hackery was needed to make both vscode and g++ happy
#ifndef __INTELLISENSE__
    if(res.compile()) return res;
#else
    if(res.compile()) return move(res);
#endif

    string message = fmt::format("In file {}: \n{}", filename, res.error_message());
    return Error::with_message(message);
}

GLShader::GLShader(GLShader&& shader)
    : m_type(shader.m_type)
    , m_source(move(shader.m_source))
    , m_glid(shader.m_glid)
    , m_is_compiled(shader.m_is_compiled)
    {
        shader.m_glid = 0;
        shader.m_is_compiled = false;
    }

GLShader::GLShader(string_view source, Type type)
    : m_type(type)
    , m_source(source)
    {
        GLenum gl_type = (type == Type::Fragment) ? GL_FRAGMENT_SHADER : GL_VERTEX_SHADER;
        m_glid = glCreateShader(gl_type);
        GLint length = source.length();
        const char* cstr = m_source.c_str();
        glShaderSource(m_glid, 1, &cstr, &length);
    }

GLShader::~GLShader()
{
    if(m_glid == 0) return;
    glDeleteShader(m_glid);
}

GLuint GLShader::gl_id() const
{
    assert(m_is_compiled);
    return m_glid;
}

bool GLShader::compile()
{
    assert(!m_is_compiled);
    glCompileShader(m_glid);

    GLint compile_status = GL_FALSE;
    glGetShaderiv(m_glid, GL_COMPILE_STATUS, &compile_status);
    if(compile_status == GL_TRUE) {
        m_is_compiled = true;
        return true;
    }

    return false;
}

string GLShader::error_message()
{
    GLint length;
    glGetShaderiv(m_glid, GL_INFO_LOG_LENGTH, &length);
    if(length == 0) {
        return "";
    }
    vector<char> logbuf(length+1);
    glGetShaderInfoLog(m_glid, length, NULL, logbuf.data());
    return string { logbuf.begin(), logbuf.end() };
}

void _gl_uniform_set(GLuint location, int val)
{
    glUniform1iv(location, 1, &val);
}

void _gl_uniform_set(GLuint location, float val)
{
    glUniform1fv(location, 1, &val);
}

void _gl_uniform_set(GLuint location, glm::vec2 val)
{
    glUniform2fv(location, 1, &val[0]);
}

void _gl_uniform_set(GLuint location, glm::vec3 val)
{
    glUniform3fv(location, 1, &val[0]);
}

void _gl_uniform_set(GLuint location, glm::ivec2 val)
{
    glUniform2iv(location, 1, &val[0]);
}

void _gl_uniform_set(GLuint location, glm::ivec3 val)
{
    glUniform3iv(location, 1, &val[0]);
}

template<typename T>
class GLUniform
{
    friend class GLProgram;
public:
    ~GLUniform() { }

    void set_value(T val)
    {
        GLint program_in_use;
        glGetIntegerv(GL_CURRENT_PROGRAM, &program_in_use);
        assert(program_in_use == (GLint)m_program_id);

        _gl_uniform_set(m_location, val);
    }
private:
    GLUniform(GLint location, GLint program_id)
        : m_location(location), m_program_id(program_id) { }
    GLuint m_location;
    GLuint m_program_id;
};

class GLProgram
{
public:
    // TODO: Add a varargs of shader references here
    GLProgram();
    ~GLProgram();
    GLProgram(GLProgram&& program);

    void attach(GLShader const& shader);
    ErrorOr<void> link();
    void use(function<void()> body);

    template<typename T>
    GLUniform<T> get_uniform(string_view name)
    {
        assert(m_is_linked);

        string name_copy(name);
        GLint location = glGetUniformLocation(m_glid, name_copy.c_str());

        if(location == GL_INVALID_OPERATION || location == GL_INVALID_VALUE)
            assert(false && "Can't create uniform: Invalid program object");

        if(location < 0)
            fmt::print(stderr, "WARNING: Invalid uniform name {} (maybe optimized out?)\n", name);
        return GLUniform<T> { location, (GLint)m_glid };
    }

private:
    GLuint m_glid { 0 };
    vector<GLuint> m_attached_shaders;
    bool m_is_linked { false };
};

GLProgram::GLProgram()
{
    m_glid = glCreateProgram();
}

GLProgram::GLProgram(GLProgram&& program)
    : m_glid(program.m_glid)
    , m_attached_shaders(program.m_attached_shaders)
    , m_is_linked(program.m_is_linked)
    {
        program.m_glid = 0;
        program.m_attached_shaders.clear();
        program.m_is_linked = false;
    }

GLProgram::~GLProgram()
{
    if(m_glid == 0) return;
    glDeleteProgram(m_glid);
}

void GLProgram::attach(GLShader const& shader)
{
    assert(!m_is_linked);

    glAttachShader(m_glid, shader.gl_id());
    m_attached_shaders.push_back(shader.gl_id());
}

ErrorOr<void> GLProgram::link()
{
    assert(!m_attached_shaders.empty());
    assert(!m_is_linked);

    Defer detach([this] {
        for(auto shader_id : m_attached_shaders)
            glDetachShader(m_glid, shader_id);
        m_attached_shaders.clear();
    });

    glLinkProgram(m_glid);
    GLint link_status = GL_FALSE;
    glGetProgramiv(m_glid, GL_LINK_STATUS, &link_status);
    if(link_status == GL_FALSE) {
        GLint length;
        glGetProgramiv(m_glid, GL_INFO_LOG_LENGTH, &length);
        vector<char> logbuf(length+1);
        glGetProgramInfoLog(m_glid, length, NULL, logbuf.data());
        string error_message { logbuf.begin(), logbuf.end() };
        return Error::with_message(error_message);
    }

    m_is_linked = true;
    return {};
}

void GLProgram::use(function<void()> body)
{
    assert(m_is_linked);
    GLint program_in_use;
    glGetIntegerv(GL_CURRENT_PROGRAM, &program_in_use);
    assert(program_in_use == 0);

    glUseProgram(m_glid);
    body();
    glUseProgram(0);
}

class GLEWLoader
{
public:
    GLEWLoader();
    ~GLEWLoader() { }

    bool is_supported(string_view extension_name) const;
    void require(string_view extension_name);
private:
};

GLEWLoader::GLEWLoader()
{
    auto rc = glewInit();
    if(rc != GLEW_OK) {
        string msg = reinterpret_cast<const char*>(glewGetErrorString(rc));
        fmt::print(stderr, "Error loading glew: {}\n", msg);
        exit(1);
    }
}

bool GLEWLoader::is_supported(string_view extension_name) const
{
    string copy(extension_name);
    return glewIsSupported(copy.c_str());
}

void GLEWLoader::require(string_view extension_name)
{
    if(!is_supported(extension_name)) {
        fmt::print(stderr, "Required extension not found: {}\n", extension_name);
        exit(1);
    }
}
template<typename T>
class GLArrayBuffer
{
public:
    GLArrayBuffer()
    {
        glGenBuffers(1, &m_glid);
    }
    GLArrayBuffer(GLArrayBuffer<T>&& o)
        : m_glid(o.m_glid)
        {
            // Make sure the other buffer is not being used at the moment
            assert(!o.m_is_bound);

            o.m_glid = 0;
        }
    ~GLArrayBuffer()
    {
        assert(!m_is_bound);

        if(m_glid)
            glDeleteBuffers(1, &m_glid);
    }

    void bind(std::function<void()> body)
    {
        assert(m_glid);

        // Make sure no other buffer is bound
        GLint currently_bound_buffer;
        glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &currently_bound_buffer);
        assert(!currently_bound_buffer);

        m_is_bound = true;
        glBindBuffer(GL_ARRAY_BUFFER, m_glid);

        body();

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        m_is_bound = false;
    }

    template<size_t N>
    void load(array<T, N> data)
    {
        assert(m_glid);
        assert(m_is_bound);

        glBufferData(GL_ARRAY_BUFFER, data.size()*sizeof(T), data.data(), GL_STATIC_DRAW);
        assert(!glGetError());
    }

    void load(vector<T> data)
    {
        assert(m_glid);
        assert(m_is_bound);

        glBufferData(GL_ARRAY_BUFFER, data.size()*sizeof(T), data.data(), GL_STATIC_DRAW);
        assert(!glGetError());
    }
private:
    GLuint m_glid { 0 };
    bool m_is_bound { false };
};

// returns the size od screen in characters
glm::ivec2 update_title_for_scale(SDLWindow& window, glm::vec2 scaled_character_size)
{
    glm::vec2 size_in_chars = glm::vec2(2.0f) / scaled_character_size;
    size_t rows = (int)size_in_chars.y;
    size_t cols = (int)size_in_chars.x;
    window.set_title(fmt::format("term thingy [{}x{}]", cols, rows));

    return size_in_chars;
}

class VisualBell
{
public:
    VisualBell(std::string const& icon_filename);
    ~VisualBell();

    void update_time(float time);
    void use(std::function<void()> body);
    void ring();
    // this should probably be configurable. 1.0 is the upper bound of the bell animation duration
    // (in seconds)
    bool recently_rang() const { return m_current_time - m_bell_time < 1.0; }
private:
    void load_shaders();

    GLuint m_texture;
    float m_current_time { 0.0 };
    float m_bell_time { -1000.0 };
    // TODO: Send bell image position/size as uniform

    GLProgram m_program;
    optional<GLUniform<float>> m_time_uniform;
    optional<GLUniform<float>> m_last_bell_time_uniform;
};

VisualBell::VisualBell(std::string const& icon_filename)
{
    GLint old_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &old_texture);

    glGenTextures(1, &m_texture);
    glBindTexture(GL_TEXTURE_2D, m_texture);

    int w, h, c;
    void* data = stbi_load(icon_filename.c_str(), &w, &h, &c, 4);
    assert(c == 4);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
        w, h, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    stbi_image_free(data);

    glBindTexture(GL_TEXTURE_2D, old_texture);

    load_shaders();
}

VisualBell::~VisualBell()
{
    glDeleteTextures(1, &m_texture);
}

void VisualBell::load_shaders()
{
    auto vertex_path = get_resource_path("bell_vertex.glsl");
    auto fragment_path = get_resource_path("bell_frag.glsl");
    assert(vertex_path);
    assert(fragment_path);

    auto vertex_shader_or_error = GLShader::from_file(vertex_path, GLShader::Type::Vertex);
    if(vertex_shader_or_error.is_error()) {
        fmt::print(stderr, "Vertex Shader Compile Error: {}\n", vertex_shader_or_error.error().message());
        exit(1);
    }
    auto fragment_shader_or_error = GLShader::from_file(fragment_path, GLShader::Type::Fragment);
    if(fragment_shader_or_error.is_error()) {
        fmt::print(stderr, "Fragment Shader Compile Error: {}\n", fragment_shader_or_error.error().message());
        exit(1);
    }

    auto vertex_shader = vertex_shader_or_error.release_value();
    auto fragment_shader = fragment_shader_or_error.release_value();
    m_program.attach(vertex_shader);
    m_program.attach(fragment_shader);

    auto maybe_link_error = m_program.link();
    if(maybe_link_error.is_error()) {
        fmt::print(stderr, "Bell shader link Error: {}\n", maybe_link_error.error().message());
        exit(1);
    }

    m_last_bell_time_uniform = m_program.get_uniform<float>("last_bell_time");
    m_time_uniform = m_program.get_uniform<float>("time");
}

void VisualBell::update_time(float time)
{
    m_current_time = time;
}

void VisualBell::ring()
{
    m_bell_time = m_current_time;
}

void VisualBell::use(function<void()> body)
{
    GLint old_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &old_texture);

    glBindTexture(GL_TEXTURE_2D, m_texture);
    m_program.use([&](){
        m_last_bell_time_uniform->set_value(m_bell_time);
        m_time_uniform->set_value(m_current_time);
        body();
    });

    glBindTexture(GL_TEXTURE_2D, old_texture);
}

string get_default_shell()
{
    uid_t euid = geteuid();

    struct passwd pwd;
    struct passwd* result;

    char buf[16384];

    getpwuid_r(euid, &pwd, buf, 1024, &result);
    assert(result != NULL);

    return pwd.pw_shell;
}

int main(int argc, char **argv)
{
    CLI::App app("Terminal Emulator Thingy");

    bool dont_attach_shell = false;
    app.add_flag("--no-shell", dont_attach_shell);

    // TODO: Maybe add a better config method than cli arguments?
    string shell_program = get_default_shell();
    app.add_option("-s,--shell", shell_program);

    bool font_antialiasing = true;
    app.add_flag("-A{false},--no-font-antialiasing{false}", font_antialiasing);
    string font = ":family=monospace";
    app.add_option("-f,--font", font);

    string terminal_name = "ansi";
    app.add_option("-t,--term", terminal_name);

    // TODO: Calculate this from pixel size of font 
    // size = (pixel_height/window_height)
    float scale = 0.06;
    app.add_option("--scale", scale);

    optional<string> working_directory;
    app.add_option("-w,--working-directory", working_directory);

    app.add_flag("-q,--quiet", QUIET_MODE);

    CLI11_PARSE(app, argc, argv);

    setlocale(LC_ALL, "en_US.utf8");

    PtyDevice pty;
    pty.set_terminal_name(terminal_name);
    if(working_directory) {
        pty.set_working_directory(*working_directory);
    } else {
        char cwd_buf[2];
        getcwd(cwd_buf, 2);
        if (cwd_buf[0] == '/' && cwd_buf[1] == '\0')
            pty.set_working_directory(getenv("HOME"));
    }
    if(!dont_attach_shell) pty.attach(shell_program);

    glm::ivec2 window_size { 800, 600 };
    SDLWindow window("terminal thingy", window_size);
    window.set_icon(get_resource_path("logo_256px.png"));

    // TODO: Introduce a renderer class to handle all these GL related things
    GLEWLoader gl_extensions;
#ifndef __APPLE__
    gl_extensions.require("GL_ARB_debug_output");
    gl_extensions.require("GL_ARB_draw_instanced");
    glDebugMessageCallback(glDebugCallback, NULL);
#endif

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0., 0., 0., 1.);

    FontLoader font_loader;
    auto maybe_font_path = font_loader.locate_font(font);
    if(!maybe_font_path) {
        fmt::print(stderr, "Could not find monospaced font in system\n");
        exit(1);
    }

    assert(FontAtlas::gl_supports_atlas_texture());
    FontAtlas atlas(*maybe_font_path, font_antialiasing);
    atlas.generate_ascii_atlas();

    // The font is assumed to be monospaced, since if it was not, a simple width*height in chars calculation would be impossible
    glm::vec2 size_of_char = scale  * atlas.monospace_character_size();

    auto size_in_chars = update_title_for_scale(window, size_of_char);
    size_t rows = size_in_chars.y;
    size_t cols = size_in_chars.x;
    ScrollbackBuffer scrollback_buffer(1000, cols);
    ScreenBuffer screen_buffer(scrollback_buffer, rows, cols);
    BufferDisplay buffer_display(screen_buffer, scrollback_buffer, atlas, scale);
    buffer_display.regenerate_character_info(instanced_info);
    pty.notify_size(size_in_chars);

    VisualBell bell(get_resource_path("bell.png"));

    LineBuffer line;

    GLVertexArray vertex_array;
    GLArrayBuffer<PerVertexInfo> vertex_buffer;
    GLArrayBuffer<PerCharacterInfo> instanced_buffer;

    vertex_buffer.bind([&](){
        vertex_buffer.load(vertex_buffer_data);
    });
    instanced_buffer.bind([&](){
        instanced_buffer.load(instanced_info);
    });

    const char* vertex_shader_path = get_resource_path("vertex.glsl");
    const char* fragment_shader_path = get_resource_path("frag.glsl");
    assert(vertex_shader_path);
    assert(fragment_shader_path);

    auto vertex_shader_or_error = GLShader::from_file(vertex_shader_path, GLShader::Type::Vertex);
    if(vertex_shader_or_error.is_error()) {
        fmt::print(stderr, "Vertex Shader Compile Error: {}\n", vertex_shader_or_error.error().message());
        exit(1);
    }

    auto fragment_shader_or_error = GLShader::from_file(fragment_shader_path, GLShader::Type::Fragment);
    if(fragment_shader_or_error.is_error()) {
        fmt::print(stderr, "Fragment Shader Compile Error: {}\n", fragment_shader_or_error.error().message());
        exit(1);
    }

    auto vertex_shader = vertex_shader_or_error.release_value();
    auto fragment_shader = fragment_shader_or_error.release_value();
    GLProgram program;
    program.attach(vertex_shader);
    program.attach(fragment_shader);

    auto maybe_link_error = program.link();
    if(maybe_link_error.is_error()) {
        fmt::print(stderr, "Shader Link Error: {}\n", maybe_link_error.error().message());
        exit(1);
    }

    const char* bg_vertex_shader_path = get_resource_path("bg_vertex.glsl");
    const char* bg_fragment_shader_path = get_resource_path("bg_frag.glsl");
    assert(bg_vertex_shader_path);
    assert(bg_fragment_shader_path);

    GLProgram bg_program;
    {
        auto vertex_shader_or_error = GLShader::from_file(bg_vertex_shader_path, GLShader::Type::Vertex);
        if(vertex_shader_or_error.is_error()) {
            fmt::print(stderr, "Vertex Shader Compile Error: {}\n", vertex_shader_or_error.error().message());
            exit(1);
        }

        auto fragment_shader_or_error = GLShader::from_file(bg_fragment_shader_path, GLShader::Type::Fragment);
        if(fragment_shader_or_error.is_error()) {
            fmt::print(stderr, "Fragment Shader Compile Error: {}\n", fragment_shader_or_error.error().message());
            exit(1);
        }

        auto vertex_shader = vertex_shader_or_error.release_value();
        auto fragment_shader = fragment_shader_or_error.release_value();
        bg_program.attach(vertex_shader);
        bg_program.attach(fragment_shader);

        auto maybe_link_error = bg_program.link();
        if(maybe_link_error.is_error()) {
            fmt::print(stderr, "Background shader link Error: {}\n", maybe_link_error.error().message());
            exit(1);
        }
    }

    auto cursor_index_uniform = program.get_uniform<int>("cursor_index");
    auto time_uniform = program.get_uniform<float>("time");
    auto time_since_last_action_uniform = program.get_uniform<float>("time_since_last_action");

    auto bg_cursor_index_uniform = bg_program.get_uniform<int>("cursor_index");
    auto bg_time_uniform = bg_program.get_uniform<float>("time");
    auto bg_time_since_last_action_uniform = bg_program.get_uniform<float>("time_since_last_action");

    float current_time = 0.0;
    float time_of_last_action = 0.0;

    vertex_array.bind([&](){
        vertex_buffer.bind([&](){
            vertex_array.define_attribute(0, GLVertexAttribute(glm::vec3, PerVertexInfo, vertex_position));
            vertex_array.define_attribute(1, GLVertexAttribute(glm::vec2, PerVertexInfo, texture_coords));
        });
        instanced_buffer.bind([&](){
            vertex_array.define_attribute(2, GLInstancedAttribute(glm::vec2, PerCharacterInfo, draw_pos));
            vertex_array.define_attribute(3, GLInstancedAttribute(glm::vec2, PerCharacterInfo, draw_size));
            vertex_array.define_attribute(4, GLInstancedAttribute(glm::vec2, PerCharacterInfo, font_pos));
            vertex_array.define_attribute(5, GLInstancedAttribute(glm::vec2, PerCharacterInfo, font_size));
            vertex_array.define_attribute(6, GLInstancedAttribute(glm::vec3, PerCharacterInfo, fg_color));
            vertex_array.define_attribute(7, GLInstancedAttribute(glm::vec3, PerCharacterInfo, bg_color));
            vertex_array.define_attribute(8, GLInstancedAttribute(glm::vec2, PerCharacterInfo, bg_draw_pos));
            vertex_array.define_attribute(9, GLInstancedAttribute(glm::vec2, PerCharacterInfo, bg_draw_size));
        });
    });

    SDL_StartTextInput();
    auto main_loop = window.event_loop();

    Utf8Stream keyboard_input_stream;

    bool left_ctrl_down = false;
    bool right_ctrl_down = false;

    bool left_super_down = false;
    bool right_super_down = false;

    MouseSelection mouse_selection(window.window_size(), instanced_info);

    auto handle_keyboard_shortcut = [&](SDL_Keycode keycode) {
        switch(keycode) {
        case SDLK_c: {
            auto range = mouse_selection.range();
            if(range.length() > 0) {
                auto text = screen_buffer.get_text_in_range(range);
                SDL_SetClipboardText(text.c_str());
            }
            break; }
        case SDLK_v: {
            auto text = SDL_GetClipboardText();
            if(!text) break;
            // TODO: Handle unicode here as well
            //       (Have a insert_zero_terminated method in screen buffer, maybe?)
            std::string str(text);

            if(pty.in_echo_mode()) {
                for(auto ch : str) screen_buffer.insert_char(ch);
            }

            if(pty.in_canonical_mode())
                line.insert_zero_terminated(text);
            else
                pty.write_zero_terminated(text);

            break; }
        }
    };

    main_loop.register_event(SDL_TEXTINPUT, [&](auto evt) {
        time_of_last_action = current_time;

        optional<Utf8Result> curr_result;
        for(size_t i = 0; i<32; i++) {
            if(!evt.text.text[i]) break;
            curr_result.emplace(keyboard_input_stream.feed(evt.text.text[i]));
        }
        assert(curr_result);
        if(curr_result->type() != Utf8Result::Type::Codepoint) {
            fmt::print(stderr, "Invalid utf-8 input, this should not happen\n");
            return;
        }

        auto codepoint = curr_result->codepoint();
        if(pty.in_echo_mode())
            screen_buffer.insert_char(codepoint);

        if(pty.in_canonical_mode())
            line.insert_zero_terminated(evt.text.text);
        else
            pty.write_zero_terminated(evt.text.text);

        instanced_buffer.bind([&](){
            instanced_buffer.load(instanced_info);
        });
    });
    main_loop.register_event(SDL_KEYDOWN, [&](auto evt) {
        time_of_last_action = current_time;
        if(left_super_down || right_super_down) {
            handle_keyboard_shortcut(evt.key.keysym.sym);
            return;
        }

        switch(evt.key.keysym.sym) {
        case SDLK_BACKSPACE:
            if(pty.in_echo_mode())
                screen_buffer.backspace();

            if(pty.in_canonical_mode())
                line.backspace();
            else
                pty.send_byte(0x08);

            break;
        case SDLK_DELETE:
            if(pty.in_echo_mode())
                screen_buffer.delete_characters(1);

            // TODO: Handle delete key in canonical mode
            if(!pty.in_canonical_mode()) {
                // NOTE: I have no idea why it's necessary to send an arrow key before the delete character
                pty.write_zero_terminated("\033[C");
                pty.send_byte(0x7F);
            }

            break;
        case SDLK_RETURN:
            if(pty.in_echo_mode()) screen_buffer.newline();
            if(pty.in_canonical_mode()) {
                line.insert_zero_terminated("\n");
                pty.write_line(line);
                line.clear();
            } else {
                pty.send_byte('\n');
            }
            break;
        case SDLK_TAB:
            if(pty.in_echo_mode()) screen_buffer.tab();

            if(pty.in_canonical_mode()) line.insert_zero_terminated("\t");
            else pty.send_byte('\t');
            break;
        case SDLK_UP:
            if(pty.in_canonical_mode()) screen_buffer.move_up();
            else pty.write_zero_terminated("\033[A");
            break;
        case SDLK_DOWN:
            if(pty.in_canonical_mode()) screen_buffer.move_down();
            else pty.write_zero_terminated("\033[B");
            break;
        case SDLK_LEFT:
            if(pty.in_canonical_mode()) screen_buffer.move_left();
            else pty.write_zero_terminated("\033[D");
            break;
        case SDLK_RIGHT:
            if(pty.in_canonical_mode()) screen_buffer.move_right();
            else pty.write_zero_terminated("\033[C");
            break;
        case SDLK_LCTRL:
            left_ctrl_down = true;
            break;
        case SDLK_RCTRL:
            right_ctrl_down = true;
            break;
        case SDLK_LGUI:
            left_super_down = true;
            break;
        case SDLK_RGUI:
            right_super_down = true;
            break;
        }
    });
    main_loop.register_event(SDL_KEYUP, [&](auto evt) {
        switch(evt.key.keysym.sym) {
        case SDLK_LCTRL:
            left_ctrl_down = false;
            break;
        case SDLK_RCTRL:
            right_ctrl_down = false;
            break;
        case SDLK_LGUI:
            left_super_down = false;
            break;
        case SDLK_RGUI:
            right_super_down = false;
            break;

        default:
            char sym = evt.key.keysym.sym;

            bool any_ctrl_down = left_ctrl_down || right_ctrl_down;
            bool can_send_ctrl_seq = line.size() == 0;
            // TODO: Handle shift in this case as well
            if(any_ctrl_down && can_send_ctrl_seq && isalpha(sym)) {
                screen_buffer.newline();
                // NOTE: SDL's keysym enum has a nice property that alphabetic
                //      keysym's are just that char in ASCII
                uint8_t byte = (sym - 'a' + 1);
                pty.send_byte(byte);
                return;
            }
            break;
        }
    });

    main_loop.register_event(SDL_MOUSEWHEEL, [&](SDL_Event evt) {
        auto wheel_y = evt.wheel.y;
        if(left_ctrl_down || right_ctrl_down ||
            left_super_down || right_super_down) {
            if(wheel_y < 0) buffer_display.zoom_out();
            else if(wheel_y > 0) buffer_display.zoom_in();

            auto size_of_char = buffer_display.current_scale() * atlas.monospace_character_size();

            auto size_in_chars = update_title_for_scale(window, size_of_char);
            screen_buffer.resize(size_in_chars);
            buffer_display.regenerate_character_info(instanced_info);
            pty.notify_size(size_in_chars);
        } else {
            // TODO: Make it possible to scroll by more than one line
            if(wheel_y < 0) buffer_display.scroll_down();
            else if(wheel_y > 0) buffer_display.scroll_up();
        }

        instanced_buffer.bind([&](){
            instanced_buffer.load(instanced_info);
        });
    });
    main_loop.register_event(SDL_WINDOWEVENT, [&](auto evt) {
        if(evt.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
            glViewport(0, 0, (GLsizei)evt.window.data1, (GLsizei)evt.window.data2);

            // if window size gets scaled twice, glyph scale needs to halve
            // window scaling = new window size / old window size
            // => glyph scaling = float(old window size) / float(new window size)
            auto old_window_size = window.window_size();
            auto new_window_size = glm::ivec2(evt.window.data1, evt.window.data2);
            window.set_window_size(new_window_size);
            mouse_selection.set_window_size(new_window_size);
            auto scale = glm::vec2(old_window_size) / glm::vec2(new_window_size);

            // we scale it by that amount, get new size in chars
            // update window title, resize screen buffer
            buffer_display.scale_by(scale);
            auto new_size_in_chars = update_title_for_scale(window, buffer_display.current_scale() * atlas.monospace_character_size());
            screen_buffer.resize(new_size_in_chars);
            pty.notify_size(new_size_in_chars);
        }
    });

    main_loop.register_event(SDL_MOUSEBUTTONDOWN, [&](SDL_Event evt) {
        if(evt.button.button != SDL_BUTTON_LEFT) return;

        mouse_selection.start({ evt.button.x, evt.button.y });
        screen_buffer.set_selection(mouse_selection.range());
    });

    main_loop.register_event(SDL_MOUSEMOTION, [&](SDL_Event evt) {
        mouse_selection.update({ evt.motion.x, evt.motion.y });
        screen_buffer.set_selection(mouse_selection.range());
    });

    main_loop.register_event(SDL_MOUSEBUTTONUP, [&](SDL_Event evt) {
        if(evt.button.button == SDL_BUTTON_LEFT) {
            mouse_selection.end({ evt.button.x, evt.button.y });
            screen_buffer.set_selection(mouse_selection.range());
        } else if(evt.button.button == SDL_BUTTON_RIGHT) {
            auto range = mouse_selection.range();
            if(range.length() > 0) {
                auto text = screen_buffer.get_text_in_range(range);
                SDL_SetClipboardText(text.c_str());
            }
        } else if(evt.button.button == SDL_BUTTON_MIDDLE) {
            auto text = SDL_GetClipboardText();
            if(!text) return;
            // TODO: Handle unicode here as well
            //       (Have a insert_zero_terminated method in screen buffer, maybe?)
            std::string str(text);

            if(pty.in_echo_mode()) {
                for(auto ch : str) screen_buffer.insert_char(ch);
            }

            if(pty.in_canonical_mode())
                line.insert_zero_terminated(text);
            else
                pty.write_zero_terminated(text);
        }
    });

    main_loop.run([&](uint64_t ticks_ms){
        // I'm not sure if doing that in the main thread is the best idea
        // TODO: Try moving it to a separate thread and see if this reduces cpu usage further
        auto poll_result = pty.poll_data();

        if(poll_result == PtyDevice::PollResult::HasData) {
            pty.read_into(screen_buffer);
        } else if(poll_result == PtyDevice::PollResult::HungUp) {
            fmt::print("Shell process hung up\n");
            main_loop.stop();
        }

        // No need to do anything if the window is not visible
        if(is_window_visible() == UncertainBool::DefinitelyFalse) return;

        current_time = ticks_ms / 1000.f;
        bell.update_time(current_time);

        bool should_redraw = false;
        if(buffer_display.dirty()) {
            buffer_display.regenerate_character_info(instanced_info);
            instanced_buffer.bind([&](){
                instanced_buffer.load(instanced_info);
            });
            should_redraw = true;
        }

        if(screen_buffer.should_beep()) {
            bell.ring();
            screen_buffer.mark_as_beeped();
            should_redraw = true;
        }

        if(bell.recently_rang()) should_redraw = true;

        // optimize for the case where nothing has changed
        // don't redraw anything in that case
        if(!should_redraw) return;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindTexture(GL_TEXTURE_2D, atlas.texture_id());

        bg_program.use([&]() {
            bg_cursor_index_uniform.set_value(screen_buffer.cursor_index());
            bg_time_uniform.set_value(current_time);
            bg_time_since_last_action_uniform.set_value(current_time - time_of_last_action);

            vertex_array.bind([&](){
                glDrawArraysInstanced(GL_TRIANGLES, 0, 6, instanced_info.size());
            });
        });

        program.use([&]() {
            cursor_index_uniform.set_value(screen_buffer.cursor_index());
            time_uniform.set_value(current_time);
            time_since_last_action_uniform.set_value(current_time - time_of_last_action);

            vertex_array.bind([&](){
                glDrawArraysInstanced(GL_TRIANGLES, 0, 6, instanced_info.size());
            });
        });

        bell.use([&]() {
            // NOTE: We can use the same vertex data since we're still going to draw a textured quad
            vertex_array.bind([&](){
                glDrawArrays(GL_TRIANGLES, 0, 6);
            });
        });

        window.draw();
    });

    return 0;
}
