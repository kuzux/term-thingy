#pragma once
#include "pch.h"
#include "ScreenBuffer.h"
#include "gl_buffer_defs.h"

class BufferDisplay
{
public:
    BufferDisplay(ScreenBuffer& screen_buffer, ScrollbackBuffer& scrollback_buffer, FontAtlas& atlas, float scale);
    ~BufferDisplay() = default;

    bool dirty() const;
    void regenerate_character_info(std::vector<PerCharacterInfo>& buffer);
    glm::vec2 current_scale() const { return m_scale; }

    void scale_by(glm::vec2 scale);
    void zoom_in() { scale_by(glm::vec2(1.1)); }
    void zoom_out() { scale_by(glm::vec2(0.9)); }
    void resize(glm::ivec2 new_size);

    void scroll_up();
    void scroll_down();
private:
    ScreenBuffer& m_screen_buffer;
    ScrollbackBuffer& m_scrollback_buffer;
    int m_scroll_offset { 0 };
    FontAtlas& m_atlas;

    glm::vec2 m_scale;
    bool m_dirty { false };
};
