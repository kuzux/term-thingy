#include "platform.h"

#import <AppKit/AppKit.h>

#include <unistd.h>
#include <sys/stat.h>

#include <stdio.h>

PackageType application_packaged_as()
{
    pid_t pid = getpid();
    NSRunningApplication* app = [NSRunningApplication runningApplicationWithProcessIdentifier: pid];
    if(app != nil && [app bundleIdentifier] != nil) return PACKAGE_TYPE_OSX_APP;

    return PACKAGE_TYPE_EXECUTABLE;
}

static NSWindow* current_window = nil;

UncertainBool is_window_visible()
{
    if(current_window == nil) {
        // It was necessary to do that to acces the window if it did not have focus
        NSArray<NSWindow*>* windows = [[NSApplication sharedApplication] windows];
        if(!windows) return MAYBE;
        if([windows count] == 0) return MAYBE;

        NSWindow* window = [windows firstObject];
        if(!window) return _False;

        current_window = window;
    }

    NSWindowOcclusionState state = [current_window occlusionState];
    if(state & NSWindowOcclusionStateVisible) return DEFINITELY_TRUE;

    return DEFINITELY_FALSE;
}

const char* get_resource_path(const char* resource) 
{
    const char* c_resource_path = NULL;
    char tmpbuf[1024];

    if (application_packaged_as() == PACKAGE_TYPE_OSX_APP) {
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* bundle_path = [bundle bundlePath];
        NSString* resources_path = [bundle_path stringByAppendingPathComponent: @"Contents/Resources"];

        NSString* resource_name = [NSString stringWithUTF8String: resource];
        NSString* resource_path = [resources_path stringByAppendingPathComponent: resource_name];
        c_resource_path = [resource_path UTF8String];
    } else {
        snprintf(tmpbuf, 1024, "assets/%s", resource);
        c_resource_path = tmpbuf;
    }

    struct stat statbuf;
    int rc = stat(c_resource_path, &statbuf);

    printf("resource path: %s, rc=%d\n", c_resource_path, rc);
    
    if (!rc) return strdup(c_resource_path);
    return NULL;
}
