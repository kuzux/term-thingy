#version 330 core

in vec3 fg_color;
in vec2 uv;
in vec2 font_pos;
in vec2 font_size;

uniform sampler2D font;

out vec4 color;

void main() {
    vec2 frag_uv = font_pos+font_size*uv;
    float distance = texture(font, frag_uv).r;
    float blur = 0.02;
    float alpha = smoothstep(0.5 - blur, 0.5 + blur, distance);
    color = vec4(fg_color, alpha);
}
