#version 330 core

layout(location = 0) in vec3 pos;

layout(location = 6) in vec3 fg_color_in;
layout(location = 7) in vec3 bg_color_in;
layout(location = 8) in vec2 draw_pos;
layout(location = 9) in vec2 draw_size;

out vec3 bg_color;

uniform int cursor_index;
uniform float time;
uniform float time_since_last_action;

void main() {
    // top left corner of the character
    vec2 center = draw_pos;

    // vertex's position (not the center of the character, but a corner of it)
    vec2 vertex_pos = center;
    vertex_pos += draw_size * pos.xy;

    gl_Position = vec4(vertex_pos, 0.0, 1.0);

    // this shouldn't be too big of a performance penalty
    if(gl_InstanceID == cursor_index && int(time_since_last_action)%2 == 0) {
        bg_color = fg_color_in;
    } else {
        bg_color = bg_color_in;
    }
}
