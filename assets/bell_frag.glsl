#version 330 core

uniform float time;
uniform float last_bell_time;

in vec2 uv;
out vec4 color;
uniform sampler2D tex;

void main() {
    float time_since_last_bell = time - last_bell_time;
    float alpha = 1.0 - pow(1.5*time_since_last_bell, 3);
    alpha = max(alpha, 0.0);
    vec4 tex_color = texture(tex, uv);
    tex_color.a *= alpha;
    color = tex_color;
}