#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv_in;

out vec2 uv;

void main() {
    vec2 bell_pos = vec2(0.87, 0.87);
    vec2 bell_size = vec2(0.1, 0.11);

    vec3 pos3 = vec3(bell_pos + pos.xy * bell_size, 0.0);
    gl_Position = vec4(pos3, 1.0);
    uv = uv_in;
}