#pragma once
#include "pch.h"

// Represents a [start, end) or (end, start] range
template<typename T>
struct Range
{
    T start;
    T end;

    Range(T start, T end) : start(start), end(end) { }

    bool contains(T o) const {
        if(start <= end) return o >= start && o < end;
        return o <= start && o > end;
    }
    T length() const { return end - start; }
};

template<typename T>
struct fmt::formatter<Range<T>>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(const Range<T>& s, FormatContext& ctx)
    {
        return format_to(ctx.out(), "[{}, {})", s.start, s.end);
    }
};

template<typename T>
bool operator==(Range<T> const& a, Range<T> const& b)
{
    return a.start == b.start && a.end == b.end;
}

template<typename T>
bool operator!=(Range<T> const& a, Range<T> const& b)
{
    return a.start != b.start || a.end != b.end;
}
