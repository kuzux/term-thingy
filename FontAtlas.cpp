#include "pch.h"
#include "FontAtlas.h"
using namespace std;

// defined in main.cpp
extern bool QUIET_MODE;

FontAtlas::FontAtlas(string const& filename, bool font_antialiasing)
    : m_use_sdf(font_antialiasing) {
    glGenTextures(1, &m_texture);

    glBindTexture(GL_TEXTURE_2D, m_texture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    GLint max_size;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_size);
    m_atlas_size = max_size;

    int requested_font_height = 128;
    if(m_atlas_size >= 8192)
        requested_font_height = 256;

    assert(!FT_Init_FreeType(&m_library));
    assert(!FT_New_Face(m_library, filename.c_str(), 0, &m_face));
    assert(!FT_Set_Pixel_Sizes(m_face, 0, requested_font_height));

    // Calculate the actual font height we've got in pixels
    int bbox_ymax = FT_MulFix(m_face->bbox.yMax, m_face->size->metrics.y_scale) >> 6;
    int bbox_ymin = FT_MulFix(m_face->bbox.yMin, m_face->size->metrics.y_scale) >> 6;
    m_font_height_px = bbox_ymax - bbox_ymin;

    if(!QUIET_MODE) {
        fmt::print("Requested font height: {}px\n", requested_font_height);
        fmt::print("Actual font height: {}px\n", m_font_height_px);
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED,
        m_atlas_size, m_atlas_size, 0,
        GL_RED, GL_UNSIGNED_BYTE, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

FontAtlas::~FontAtlas()
{
    glDeleteTextures(1, &m_texture);
    FT_Done_Face(m_face);
    FT_Done_FreeType(m_library);
}

bool FontAtlas::gl_supports_atlas_texture()
{
    GLint max_size;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_size);
    return 4096 <= max_size; // min required texture size
}

void FontAtlas::generate_ascii_atlas()
{
    for(int codepoint = 32; codepoint < 128; codepoint++)
        m_ascii_metrics[codepoint - 32] = generate_glyph(codepoint);
}

void FontAtlas::generate_glyphs(unordered_set<char32_t> codepoints)
{
    for(auto cp : codepoints) {
        if(glyph_in_cache(cp)) continue;
        m_unicode_metrics[cp] = generate_glyph(cp);
    }
}

FontAtlas::GlyphMetrics FontAtlas::generate_glyph(char32_t codepoint)
{
    static constexpr int glyph_padding_px = 10;

    if(m_use_sdf) {
        auto ft_err = FT_Load_Char(m_face, codepoint, FT_LOAD_RENDER | FT_LOAD_TARGET_(FT_RENDER_MODE_SDF));
        if(ft_err)
            ft_err = FT_Load_Char(m_face, codepoint, FT_LOAD_RENDER);
        assert(!ft_err);

        ft_err = FT_Render_Glyph(m_face->glyph, FT_RENDER_MODE_SDF);
        if(ft_err)
            ft_err = FT_Render_Glyph(m_face->glyph, FT_RENDER_MODE_NORMAL);
        assert(!ft_err);
    } else {
        assert(!FT_Load_Char(m_face, codepoint, FT_LOAD_RENDER));
        assert(!FT_Render_Glyph(m_face->glyph, FT_RENDER_MODE_NORMAL));
    }

    glm::ivec2 advance_px {
        m_face->glyph->advance.x >> 6,
        m_face->glyph->advance.y >> 6
    };

    if(m_next_available_position_in_texture.x + advance_px.x >= m_atlas_size) {
        // need to render the glyph in a new line of atlas
        m_next_available_position_in_texture.x = 0;
        m_next_available_position_in_texture.y += m_font_height_px + 2*glyph_padding_px;

        // Afterwards, make sure we have enough vertical space in the atlas texture
        assert(m_next_available_position_in_texture.y + m_font_height_px < m_atlas_size);
    }

    glm::ivec2 texture_xy = m_next_available_position_in_texture;
    GlyphMetrics metrics {};

    const FT_Bitmap& bitmap = m_face->glyph->bitmap;

    // converting from points to pixels to scalable units
    metrics.advance = glm::vec2 {
        (float)advance_px.x/m_font_height_px,
        1.0 // this is the basis of our glyph measurements :)
    };
    metrics.bitmap_size = glm::vec2 {
        (float)bitmap.width/m_font_height_px,
        (float)bitmap.rows/m_font_height_px
    };
    metrics.bitmap_offset = glm::vec2 {
        (float)m_face->glyph->bitmap_left/m_font_height_px,
        1.0 - ((float)m_face->glyph->bitmap_top/m_font_height_px)
    };

    if(texture_xy.x + advance_px.x > m_atlas_size) {
        texture_xy.x = 0;
        texture_xy.y += m_font_height_px + 2*glyph_padding_px;
    }

    texture_xy.x += glyph_padding_px;
    texture_xy.y += glyph_padding_px;

    glTexSubImage2D(
        GL_TEXTURE_2D,
        0,
        texture_xy.x,
        texture_xy.y,
        bitmap.width,
        bitmap.rows,
        GL_RED,
        GL_UNSIGNED_BYTE,
        bitmap.buffer
    );

    metrics.tex_coords = glm::vec2 {
        (float)texture_xy.x/(float)m_atlas_size,
        (float)texture_xy.y/(float)m_atlas_size
    };
    metrics.tex_size = glm::vec2 {
        (float)bitmap.width/(float)m_atlas_size,
        (float)bitmap.rows/(float)m_atlas_size
    };

    m_next_available_position_in_texture.x += advance_px.x + 2*glyph_padding_px;
    return metrics;
}

bool FontAtlas::glyph_in_cache(char32_t cp) const {
    if(cp < 128) return true;
    return (m_unicode_metrics.find(cp) != m_unicode_metrics.end());
}

FontAtlas::GlyphMetrics const& FontAtlas::get_metrics_for_codepoint(char32_t cp) const
{
    if(cp < 128) {
        assert(cp >= 32);
        return m_ascii_metrics[cp - 32];
    }

    return m_unicode_metrics.at(cp);
}

glm::vec2 FontAtlas::monospace_character_size() const
{
    auto metrics = get_metrics_for_codepoint('M');
    return metrics.advance;
}
