#pragma once
#include "pch.h"

#include <ft2build.h>
#include FT_FREETYPE_H

class FontAtlas
{
public:
    struct GlyphMetrics {
        // all of these need to be scaled
        glm::vec2 advance;
        glm::vec2 bitmap_size;
        glm::vec2 bitmap_offset; // top left corner

        // not this one
        glm::vec2 tex_coords;
        glm::vec2 tex_size;
    };

    FontAtlas(std::string const& filename, bool font_antialiasing);
    ~FontAtlas();

    static bool gl_supports_atlas_texture();
    glm::vec2 monospace_character_size() const;
    void generate_ascii_atlas();
    // TODO: Evacuate rendered glyphs from atlas if necessary
    void generate_glyphs(std::unordered_set<char32_t> codepoints);
    bool glyph_in_cache(char32_t cp) const;

    GlyphMetrics const& get_metrics_for_codepoint(char32_t cp) const;
    GLuint texture_id() const { return m_texture; }
private:
    GlyphMetrics generate_glyph(char32_t codepoint);

    FT_Library m_library;
    FT_Face m_face;

    // in the range [32 - 127]
    std::array<GlyphMetrics, 96> m_ascii_metrics;
    GLuint m_texture { 0 };

    GLint m_atlas_size;
    int m_font_height_px;

    glm::ivec2 m_next_available_position_in_texture { 0, 0 };
    std::unordered_map<char32_t, GlyphMetrics> m_unicode_metrics;

    bool m_use_sdf { false };
};