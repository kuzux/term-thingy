#pragma once
#include "pch.h"

template<>
struct fmt::formatter<glm::ivec2>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(const glm::ivec2& s, FormatContext& ctx)
    {
        return format_to(ctx.out(), "({}, {})", s.x, s.y);
    }
};

template<>
struct fmt::formatter<glm::vec2>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(const glm::vec2& s, FormatContext& ctx)
    {
        return format_to(ctx.out(), "({}, {})", s.x, s.y);
    }
};

template<>
struct fmt::formatter<glm::ivec3>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(const glm::ivec3& s, FormatContext& ctx)
    {
        return format_to(ctx.out(), "({}, {}, {})", s.x, s.y, s.z);
    }
};

template<>
struct fmt::formatter<glm::vec3>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(const glm::vec3& s, FormatContext& ctx)
    {
        return format_to(ctx.out(), "({}, {}, {})", s.x, s.y, s.z);
    }
};
